<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//head
$lang['title']	 	= 'Media Monitoring Agent';
$lang['copyright']	= '&copy; 2012 PT Mediatrac Sistem Komunikasi. All rights reserved';

//login
$lang['login'] 				= 'Log In';
$lang['logout'] 			= 'Log Out';
$lang['username'] 			= 'Username';
$lang['password'] 			= 'Password';
$lang['login_msg_fail'] 	= '<font color=RED>Please Check Your Input..!</font>';
$lang['forgot_pass'] 		= 'If you forgot your password, please contact administrator.';

//menu
$lang['dashboard'] 			= 'Dashboard';
$lang['register']			= 'Register';
$lang['liste']				= 'List';
$lang['spokeperson']		= 'Quote';
$lang['monitor']		    = 'Monitor';
$lang['setting']			= 'Setting';


//footer
$lang['location']	= 'Location';
$lang['term']	 	= 'Term & Condition';
$lang['privacy']	= 'Privacy';
$lang['help']	 	= 'Help';

//leftpanel
$lang['current_user'] 	= 'Current User';
$lang['any_time'] 		= 'Any Time';
$lang['past_day'] 		= 'Past Day';
$lang['past_week'] 		= 'Past Week';
$lang['past_month']		= 'Past Month';
$lang['past_year'] 		= 'Past Year';
$lang['year']			= 'Year';
$lang['year_to']		= 'Year To';
$lang['date']			= 'Date';
$lang['date_to']		= 'Date To';
$lang['media_set']		= 'Media Set';
$lang['client_set']		= 'Client Set';
$lang['sub_client']		= 'Sub Client';
$lang['select_range']	= 'Select Range';
$lang['all']			= 'All';
$lang['best']			= 'Best';
//setting
$lang['set_panel']		= 'Setting Panel';
$lang['tone_set']		= 'Tone Set';

//button
$lang['save']			= 'Save';
$lang['cancel']			= 'Cancel';
$lang['reset']			= 'Reset';

$lang['warn_pass1']		= 'Please Fill the blank Input..!!!';
$lang['warn_pass2']		= 'New Password and Confirm Password Not Match..!!!';
$lang['warn_pass3']		= 'Old Password False..!!!';
$lang['warn_pass4']		= 'Success';
$lang['warn_pass5']		= 'Failed..!!';

//register
$lang['register_comp']	= 'Register New Company';
$lang['comp_name']		= 'Company Name';
$lang['comp_address']	= 'Company Address';
$lang['comp_phone']		= 'Company Phone';
$lang['comp_contact']	= 'Company Contact Person';
$lang['comp_email']		= 'Company Email';
$lang['comp_expired']	= 'Company Expired Date';
$lang['comp_logo']		= 'Company Logo';
$lang['comp_color']		= 'Company Color Theme';
$lang['register_agent']	= 'Register New Agent';
$lang['register_user']	= 'Register New User';
$lang['user_fullname']	= 'Full Name';
$lang['email']			= 'Email';
$lang['company']		= 'Company';
$lang['acess_menu']		= 'Acess Menu';

//list
$lang['list_comp']		= 'Company List';

//password
$lang['old_pass']		= 'Old Password';
$lang['new_pass']		= 'New Password';
$lang['confirm_pass']	= 'Confirm Password';
$lang['warn_pass1']		= 'Please Fill the blank Input..!!!';
$lang['warn_pass2']		= 'New Password and Confirm Password Not Match..!!!';
$lang['warn_pass3']		= 'Old Password False..!!!';
$lang['warn_pass4']		= 'Success Update Password..';
$lang['warn_pass5']		= 'Failed..!!';









?>
