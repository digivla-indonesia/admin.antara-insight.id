<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

//head
$lang['title']	 	= 'Media Monitoring';
$lang['copyright']	= '&copy; 2012 PT Mediatrac Sistem Komunikasi. All rights reserved';

//login
$lang['login'] 				= 'Masuk';
$lang['logout'] 			= 'Keluar';
$lang['username'] 			= 'Nama';
$lang['password'] 			= 'Kata Kunci';
$lang['login_msg_fail'] 	= '<font color=RED>Silahkan Cek Input anda..!</font>';
$lang['forgot_pass'] 		= 'Lupa Password, hubungi administrator.';

//menu
$lang['dashboard'] 			= 'Dashboard';
$lang['today_news']			= 'Berita';
$lang['search']				= 'Cari';
$lang['query'] 				= 'Query';
$lang['summary']			= 'Ringkasan';
$lang['quotes']				= 'Kutipan';
$lang['setting']			= 'Setting';


//footer
$lang['location']	= 'Lokasi';
$lang['term']	 	= 'Syarat dan Ketentuan';
$lang['privacy']	= 'Privacy';
$lang['help']	 	= 'Help';

//leftpanel
$lang['current_user'] 	= 'Pengguna Saat Ini';
$lang['any_time'] 		= 'Kapan Saja';
$lang['past_day'] 		= 'Kemarin';
$lang['past_week'] 		= 'Minggu Lalu';
$lang['past_month']		= 'Bulan Lalu';
$lang['past_year'] 		= 'Tahun Lalu';
$lang['year']			= 'Tahun';
$lang['year_to']		= 'Sampai Tahun';
$lang['select_range']	= 'Pilih';
$lang['date']			= 'Tanggal';
$lang['date_to']		= 'Sampai Tanggal';
$lang['media_set']		= 'Media Set';
$lang['client_set']		= 'Client Set';
$lang['sub_client']		= 'Sub Client';
$lang['all']			= 'Semua';
$lang['best']			= 'Terbaik';



?>
