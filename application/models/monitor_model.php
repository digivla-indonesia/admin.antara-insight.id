<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Monitor_model extends CI_Model {
	
    function get_log_grid($like, $limit, $sidx, $start_date, $end_date) {
        $agent_id = $this->session->userdata('agent_id');
        
        $this->db->select('a.log_client, b.comp_name, b.comp_status, b.comp_expired, COUNT(a.log_id) AS counter');
        $this->db->from('tbl_log AS a');
        $this->db->join('tbl_company AS b', 'a.log_client = b.client_id');
        $this->db->where('a.log_tgl >=', $start_date);
        $this->db->where('a.log_tgl <=', $end_date);
        
        $grp = 'GROUP BY `a`.`log_client`';
        
        if (!empty($limit)) {
            if (!empty($like)) {
                $this->db->where('b.agent_id = '.$agent_id.' and '.$like.' '.$grp.' '.$sidx.' '.$limit.' ');
            }else{
                $this->db->where('b.agent_id = '.$agent_id.' '.$grp.' '.$sidx.' '.$limit.'');
            }
		}else{
            if (!empty($like)) {
                $this->db->where('b.agent_id = '.$agent_id.' and '.$like.' '.$grp.' '.$sidx.'');
            }else{
                $this->db->where('b.agent_id = '.$agent_id.' '.$grp.' '.$sidx.'');
            }
		}
        
        return $this->db->get();
    }
    
    function get_log_details($client_id, $start_date, $end_date) {
        $this->db->select('c.comp_name, l.log_user_id, u.usr_comp_login AS userid, u.usr_comp_name AS username, COUNT(l.log_id) AS counter, c.comp_status, c.comp_expired');
        $this->db->from('tbl_log AS l');
        $this->db->join('tbl_user_comp AS u', 'l.log_user_id = u.usr_uid');
        $this->db->join('tbl_company AS c', 'u.comp_id = c.comp_id');
        $this->db->where('l.log_client', $client_id);
        $this->db->where('l.log_tgl >=', $start_date);
        $this->db->where('l.log_tgl <=', $end_date);
        $this->db->group_by('u.usr_comp_login');
        
        return $this->db->get();
    }
    
	function get_notes_grid($like, $limit, $sidx) {
		$agent_id = $this->session->userdata('agent_id');
        
		$this->db->select('*');
		$this->db->from('tbl_notification AS nt');
		$this->db->join('tbl_company AS co', 'nt.comp_id = co.comp_id');
        
		if (!empty($limit)) {
            if (!empty($like)) {
                $this->db->where('co.agent_id = '.$agent_id.' and '.$like.' '.$sidx.' '.$limit.' ');
            }else{
                $this->db->where('co.agent_id = '.$agent_id.' '.$sidx.' '.$limit.'');
            }
		}else{
            if (!empty($like)) {
                $this->db->where('co.agent_id = '.$agent_id.' and '.$like.' '.$sidx.'');
            }else{
                $this->db->where('co.agent_id = '.$agent_id.' '.$sidx.'');
            }
		}
        
		return $this->db->get();
	}
	
    function insert_notification($comp_id, $start_date, $end_date, $type, $notification, $status) {
        $data = array(
                        'comp_id'       => $comp_id,
                        'start_date'    => $start_date,
                        'end_date'      => $end_date,
                        'notify_type'   => $type,
                        'notify_text'   => $notification,
                        'notify_status' => $status
                    );
        
        return $this->db->insert('tbl_notification', $data);
    }
    
    function get_notification_detail($notify_id) {
        $this->db->select('*');
        $this->db->from('tbl_notification AS nt');
        $this->db->join('tbl_company AS co', 'nt.comp_id = co.comp_id');
        $this->db->where('nt.notify_id', $notify_id);
        
        return $this->db->get();
    }
    
    function update_notification($id, $type, $note, $start, $end, $status) {
        $data = array(
                    'start_date'    => $start,
                    'end_date'      => $end,
                    'notify_type'   => $type,
                    'notify_text'   => $note,
                    'notify_status' => $status,
                    'flags'         => date('Y-m-d H:i:s')
                );
        
        return $this->db->update('tbl_notification', $data, array('notify_id' => $id));
    }
    
    function delete_notification($id) {
        $this->db->where('notify_id', $id);
        
        return $this->db->delete('tbl_notification');
    }
}
?>
