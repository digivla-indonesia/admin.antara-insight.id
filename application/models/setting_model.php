<?php
class Setting_model extends CI_Model{
	
	function cek_password($usr_id, $old_pass) {
		$this->db->select('usr_agent_id');
		$this->db->where('usr_agent_id',$usr_id);
		$this->db->where('usr_agent_passwd',$old_pass);
		return $this->db->get('tbl_user_agent');
	}
	
	function update_password($usr_id, $new_pass) {
		$data = array(
			   'usr_agent_passwd' => $new_pass,
			); 
		$this->db->where('usr_agent_id', $usr_id);
		return $this->db->update('tbl_user_agent', $data);
	}
}
?>
