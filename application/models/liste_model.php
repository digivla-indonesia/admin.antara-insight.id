<?php
class Liste_model extends CI_Model
{
	
	function get_company_grid($like, $limit, $sidx)
	{
		$agent_id	= $this->session->userdata('agent_id'); 
		$this->db->select('*');
		$this->db->from('tbl_company');
		if (!empty($limit))
		{
				if (!empty($like))
				{
					$this->db->where('agent_id = '.$agent_id.' and '.$like.' '.$sidx.' '.$limit.' ');				
				}
				else
				{
					$this->db->where('agent_id = '.$agent_id.' '.$sidx.' '.$limit.'');
				}
		}
		else
		{
				if (!empty($like))
				{
					$this->db->where('agent_id = '.$agent_id.' and '.$like.' '.$sidx.'');				
				}
				else
				{
					$this->db->where('agent_id = '.$agent_id.' '.$sidx.'');
				}
		
		}
		return $this->db->get();
	}
	
	function get_user_grid($like, $limit, $sidx)
	{
		$agent_id	= $this->session->userdata('agent_id'); 
		$this->db->select('*');
		$this->db->from('tbl_user_comp as tuc');
		$this->db->join('tbl_company as tc', 'tuc.comp_id=tc.comp_id');
		$this->db->join('tbl_level_comp as tlc', 'tuc.usr_comp_level=tlc.level_id');
		if (!empty($limit))
		{
				if (!empty($like))
				{
					$this->db->where('tc.agent_id = '.$agent_id.' and '.$like.' '.$sidx.' '.$limit.' ');				
				}
				else
				{
					$this->db->where('tc.agent_id = '.$agent_id.' '.$sidx.' '.$limit.'');
				}
		}
		else
		{
				if (!empty($like))
				{
					$this->db->where('agent_id = '.$agent_id.' and '.$like.' '.$sidx.'');				
				}
				else
				{
					$this->db->where('agent_id = '.$agent_id.' '.$sidx.'');
				}
		
		}
		return $this->db->get();
	}
	
	function get_company_detail($comp_id) 
	{
		$this->db->select('*');
		$this->db->where('comp_id', $comp_id);
		return $this->db->get('tbl_company');
	}
	
	function get_user_detail($usr_comp_id) 
	{
		$this->db->select('*');
		$this->db->from('tbl_user_comp AS tuc');
		$this->db->join('tbl_company AS tc', 'tuc.comp_id=tc.comp_id');
		$this->db->where('tuc.usr_comp_id', $usr_comp_id);
		return $this->db->get();
	}
	
	function delete_user($usr_id)
	{
		$this->db->where('usr_comp_id', $usr_id);
		return $this->db->delete('tbl_user_comp');
	}
	
	function delete_company($comp_id)
	{
		$this->db->where('comp_id', $comp_id);
		return $this->db->delete('tbl_company');
	}
    
//////////////////////////////////////////////////////////////
    
    function get_newsletter_grid($like, $limit, $sidx) {
		$agent_id = $this->session->userdata('agent_id');
        
		$this->db->select('*');
		$this->db->from('tbl_alert AS al');
		$this->db->join('tbl_company AS co', 'al.comp_id = co.comp_id');
		$this->db->join('tbl_time_repeat AS tm', 'al.timerange = tm.time_id');
		
        if (!empty($limit)) {
				if (!empty($like)) {
					$this->db->where('co.agent_id = '.$agent_id.' AND '.$like.' '.$sidx.' '.$limit.' ');
				}
				else {
					$this->db->where('co.agent_id = '.$agent_id.' '.$sidx.' '.$limit.'');
				}
		}
		else {
				if (!empty($like)) {
					$this->db->where('co.agent_id = '.$agent_id.' AND '.$like.' '.$sidx.'');
				}
				else {
					$this->db->where('co.agent_id = '.$agent_id.' '.$sidx.'');
				}
		}
        
		return $this->db->get();
	}
    
    function get_newsletter_detail($alert_id) {
        $this->db->select('*');
		$this->db->from('tbl_alert AS al');
		$this->db->join('tbl_company AS co', 'al.comp_id = co.comp_id');
		$this->db->join('tbl_time_repeat AS tm', 'al.timerange = tm.time_id');
		$this->db->where('al.alert_id', $alert_id);
        
        return $this->db->get();
    }
    
    function update_newsletter($alert_id, $email, $cat_set, $cat_id, $timerange, $start, $status) {
        $data = array(
                    'alert_email'   => $email,
                    'category_set'  => $cat_set,
                    'category_id'   => $cat_id,
                    'timerange'     => $timerange,
                    'alert_start'   => $start,
                    'statuse'       => $status,
                    'flags'         => date('Y-m-d H:i:s')
                );
        
        return $this->db->update('tbl_alert', $data, array('alert_id' => $alert_id));
    }
    
    function delete_newsletter($id) {
		$this->db->where('alert_id', $id);
        
		return $this->db->delete('tbl_alert');
	}
}
?>
