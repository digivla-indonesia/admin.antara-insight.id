<?php
class Register_model extends CI_Model
{
	
	function cek_company($comp_name, $comp_id='', $agent_id) 
	{
		$this->db->select('comp_name');
		$this->db->where('comp_name', $comp_name);
		$this->db->where('agent_id', $agent_id);
		if($comp_id!='')
		{
			$this->db->where('comp_id != '.$comp_id.'');
		}
		return $this->db->get('tbl_company');
	}
	
	function insert_company($comp_name, $comp_address, $comp_phone, $comp_contact, $comp_email, $comp_color, $comp_logo, $comp_expired, $compy_status, $client_id, $agent_id)
	{
		$date_now = date('Y-m-d');
		$data = array(
			   'comp_name' => $comp_name,
			   'comp_icon' => $comp_logo,
			   'comp_coltheme' => $comp_color,
			   'comp_address' => $comp_address,
			   'comp_contact' => $comp_contact,
			   'comp_phone' => $comp_phone,
			   'comp_email' => $comp_email,
			   'comp_join' => $date_now,
			   'comp_expired' => $comp_expired,
			   'comp_status' => $compy_status,
			   'client_id' => $client_id,
			   'agent_id' => $agent_id,
			   'create_by' => $this->session->userdata('usr_agent_name'),
			);
		return $this->db->insert('tbl_company', $data);
	}
	
	function update_company($comp_id, $comp_name, $comp_address, $comp_phone, $comp_contact, $comp_email, $comp_status, $comp_color, $comp_expired, $filename)
	{
		if($filename!='')
		{
		$data = array(
			   'comp_name' => $comp_name,
			   'comp_icon' => $filename,
			   'comp_coltheme' => $comp_color,
			   'comp_address' => $comp_address,
			   'comp_phone' => $comp_phone,
			   'comp_contact' => $comp_contact,
			   'comp_email' => $comp_email,
			   'comp_status' => $comp_status,
			   'comp_expired' => $comp_expired,
			   'create_by' => $this->session->userdata('usr_agent_name'),
			);
		}
		else
		{
		$data = array(
			   'comp_name' => $comp_name,
			   'comp_coltheme' => $comp_color,
			   'comp_address' => $comp_address,
			   'comp_phone' => $comp_phone,
			   'comp_contact' => $comp_contact,
			   'comp_email' => $comp_email,
			   'comp_status' => $comp_status,
			   'comp_expired' => $comp_expired,
			   'create_by' => $this->session->userdata('usr_agent_name'),
			);
		}
		$this->db->where('comp_id', $comp_id);
		return $this->db->update('tbl_company', $data);
	}
	
	function get_company($agent_id)
	{
		$this->db->select('*');
		$this->db->from('tbl_company');
		$this->db->where('agent_id', $agent_id);
    $this->db->where('comp_status IN ("ACTIVE", "TRIAL")');
		$this->db->order_by('comp_name');
		return $this->db->get();
	}
	
	function get_menu_company()
	{		
		//$this->db->where('menu_stat', 'ACTIVE');
		$result = $this->db->get('tbl_menu_comp');
		return $result;
	}
	
	function cek_user($name, $comp_id, $usr_id='') 
	{
		$this->db->select('usr_comp_login');
		$this->db->where('usr_comp_login', $name);
		//$this->db->where('comp_id', $comp_id);
		if($usr_id!='')
		{
			$this->db->where('usr_comp_id !=', $usr_id);
		}
		return $this->db->get('tbl_user_comp');
	}
	
	function insert_user($fullname, $email, $username, $password, $comp_id, $acess, $usr_uid)
	{
		$date_now = date('Y-m-d');
		$data = array(
			   'usr_comp_login' => $username,
			   'usr_comp_passwd' => $password,
			   'usr_comp_name' => $fullname,
			   'usr_comp_email' => $email,
			   'comp_id' => $comp_id,
			   'usr_comp_level' => $acess,
			   'usr_uid' => $usr_uid,
			   'usr_join_date' => $date_now,
			);
		return $this->db->insert('tbl_user_comp', $data);
	}
	
	
	function insert_menu_user($usr_id, $men)
	{
		$data = array(
			   'usr_id' => $usr_id,
			   'menu_id' => $men,
				);
		return $this->db->insert('tbl_level_menu_comp', $data);
	}
	
	function delete_menu_user($usr_id)
	{
		$this->db->where('usr_id', $usr_id);
		return $this->db->delete('tbl_level_menu_comp');
	}	
	
	function update_user($usr_id, $fullname, $email, $username, $acessene, $password='')
	{
		if($password!='')
		{
			$data = array(
			   'usr_comp_login' => $username,
			   'usr_comp_passwd' => $password,
			   'usr_comp_name' => $fullname,
			   'usr_comp_email' => $email,
			   'usr_comp_level' => $acessene,
			);
		}
		else
		{
			$data = array(
			   'usr_comp_login' => $username,
			   'usr_comp_name' => $fullname,
			   'usr_comp_email' => $email,
			   'usr_comp_level' => $acessene,
			);
		}
		
			
		$this->db->where('usr_comp_id', $usr_id);
		return $this->db->update('tbl_user_comp', $data);
	}
	
	function get_level_company()
	{
		//$this->db->select('*');
		//$this->db->from('tbl_level_comp');
		//return $this->db->get();
		$ordqry = "SELECT * FROM tbl_level_comp ORDER BY level_name = 'Custom', level_name;";
		return $this->db->query($ordqry);
	}
	
	function cek_menu_comp($usr_id)
	{
		$this->db->select('*');
		$this->db->from('tbl_level_menu_comp');
		$this->db->where('usr_id', $usr_id);
		return $this->db->get();
	}
	
    function get_time_repeat() {
        $this->db->select('*');
        $this->db->from('tbl_time_repeat');
        return $this->db->get();
    }
    
    function get_comp_uid($comp_id) {
        $this->db->select('usr_uid');
        $this->db->from('tbl_user_comp');
        $this->db->where('comp_id', $comp_id);
        $this->db->where('usr_comp_level', 3);
        $this->db->limit(1);
        
        return $this->db->get();
    }
    
    function add_newsletter($comp_id, $email, $cat_set, $cat_id, $timerange, $start, $status) {
        $data = array(
                    'comp_id'       => $comp_id,
                    'alert_email'   => $email,
                    'category_set'  => $cat_set,
                    'category_id'   => $cat_id,
                    'timerange'     => $timerange,
                    'alert_start'   => $start,
                    'statuse'       => $status
                );
        
        return $this->db->insert('tbl_alert', $data);
    }
}
?>
