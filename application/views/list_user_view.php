<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div id="tabs2">
	<div id="data_user">
        <table id="list_user"></table>
		<div id="user_pager"></div>
	</div>
</div>

<!-- dialog -->
<div id="dialog_user"></div>
<div id="dialog_delete"></div>

<script>
	$('#tabs').tabs();
	
	$(document).ready(function() {
		jQuery("#list_user").jqGrid({
            url: '<?php echo site_url()?>/liste/get_list_user',
            mtype: "post",
            datatype: "json",
            colNames: ['No', 'Name', 'Company', 'Level', 'View', 'Delete'],
            colModel: [
                        {name:'no', index:'no', width:12, align:"center", search:false, sortable:false, resizable:false},
                        {name:'usr_comp_name', index:'usr_comp_name', search:false, width:80},
                        {name:'comp_name', index:'comp_name', search:false},
                        {name:'level_name', index:'level_name', search:false, width:24},
                        {name:'view', index:'view', width:14, align:"center", search:false, sortable:false, resizable:false},
                        {name:'delete', index:'delete', width:20, align:"center", search:false, sortable:false, resizable:false}
                    ],
            rowNum: 25,
            width: 875, 
            height: 462,
            rowList: [25,50,100],
            pager: '#user_pager',
            sortname: 'comp_name',
            viewrecords: true,
            //multiselect: true,
            caption:"User List"
		}).jqGrid('navGrid', '#user_pager',
                    {edit:false, add:false, del:false}, //options
                    {}, // add options
                    {}, // del options
                    {overlay:false}, // edit options
                    //{sopt: ['bw','eq','ne','lt','le','gt','ge','ew','cn']} // search options
                    {sopt: ['cn']} // search options
                );
        // akhir setting jqgrid =====================================================	
		
        $(document).tooltip();
		
        $("#dialog_user").dialog({
				height: 'auto',
				width: '700px',
				autoOpen: false,
				modal: true,
				position:'top',
				title: 'Detail User',
				buttons: {		
                            'Change': function() {
                                            change_user();
                                        },
                            'Close': function() {
                                            $(this).dialog('close');
                                            $('#list_user').trigger("reloadGrid");
                                        }
                        }
        });
        
        $("#dialog_delete").dialog({
				height: 'auto',
				width: '500px',
				autoOpen: false,
				modal: true,
				position:'middle',
				title: 'Delete User',
        });
	});
	
	function detail_user(id)
	{
		$.ajax({
            cache: false,
            type: 'POST',
            url: '<?php echo site_url()?>/liste/detail_user',
            data: {'user_comp_id':id},
            success: function(data) {
                            $('#dialog_user').html(data);
                            $('#dialog_user').dialog('open');
                        }
		});
	}
	
	function delete_user(id, nama)
	{
		$('#dialog_delete').dialog({
			buttons: {
                        'Delete': function() {
                                        if (id!='')
                                        {
                                            $.ajax({
                                                cache: false,
                                                type: 'POST',
                                                url: '<?php echo site_url()?>/liste/delete_user',
                                                data: {'id':id},
                                                success: function(data) {
                                                                if (data==1)
                                                                {
                                                                    $('#dialog_warn').html('<b>Saved Successfully</b>');
                                                                    $('#dialog_warn').dialog('open');
                                                                    $('#list_user').trigger("reloadGrid");
                                                                }
                                                                else
                                                                {
                                                                    $('#dialog_warn').html('<b>Saved Failed</b>');
                                                                    $('#dialog_warn').dialog('open');
                                                                    $('#list_user').trigger("reloadGrid");
                                                                }
                                                            }
                                            });
                                        }
                                        $(this).dialog('close');
                                    },
                        'Cancel': function() {
                                        $(this).dialog('close');
                                    }
                }
		});
		$('#dialog_delete').html('<b>Are You Sure Delete This User ? <br><font color=red>'+nama+'</font></b>');
		$('#dialog_delete').dialog('open');
	}
</script>
