<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div class="CSSTableGenerator" style="width:100%">
    <form method="post" action="" id="upd_notification">
        <input type="hidden" id="nt_id" value="<?=$note_id;?>"/>
        <table>
            <tr><td colspan=3>&nbsp;</td></tr>
            <tr>
                <td width="20%">Company<font color="red">&nbsp;&nbsp;*</font></td>
                <td width="2%" align="center">:</td>
                <td width="78%"><?=$company_name;?></td>
            </tr>
            <tr>
                <td width="20%">Type<font color="red">&nbsp;&nbsp;*</font></td>
                <td width="2%" align="center">:</td>
                <td width="78%">
                    <select id="note_type" style="width:200px;height:20px;color:black;margin:0;">
                        <option value="">Choose Type</option>
                        <option value="info"<?=($note_type == 'info') ? ' SELECTED' : '';?>>Information</option>
                        <option value="warn"<?=($note_type == 'warn') ? ' SELECTED' : '';?>>Warning</option>
                        <option value="alrt"<?=($note_type == 'alrt') ? ' SELECTED' : '';?>>Alert</option>
                    </select>
                </td>
            </tr>
            <tr>
                <td width="20%">Notification<font color="red">&nbsp;&nbsp;*</font></td>
                <td width="2%" align="center">:</td>
                <td width="78%"><input type="text" style="width:500px;height:25px;" id="notification" value="<?=$note_text;?>"></td>
            </tr>
            <tr>
                <td width="20%">Period<font color="red">&nbsp;&nbsp;*</font></td>
                <td width="2%" align="center">:</td>
                <td width="78%">
                    <input type="text" style="width:100px;height:25px;" id="enote_start" value="<?=$note_start;?>"/>
                    &nbsp;&nbsp;to&nbsp;&nbsp;
                    <input type="text" style="width:100px;height:25px;" id="enote_end" value="<?=$note_end;?>"/>
                </td>
            </tr>
            <tr>
                <td width="20%">Status<font color="red">&nbsp;&nbsp;*</font></td>
                <td width="2%" align="center">:</td>
                <td width="78%">
                    <input type="radio" name="note_status" id="nstatusA" value="A"<?=($note_status == 'A') ? ' CHECKED' : '';?>/>&nbsp;&nbsp;Active<br>
                    <input type="radio" name="note_status" id="nstatusI" value="I"<?=($note_status == 'I') ? ' CHECKED' : '';?>/>&nbsp;&nbsp;Inactive<br>
                </td>
            </tr>
        </table>
    </form>
</div>

<script>
	function update_notification() {
		var note_id     = $("#nt_id").val(),
            note_type   = $("#note_type").val(),
            note_text   = $("#notification").val(),
            note_start  = $("#enote_start").val(),
            note_end    = $("#enote_end").val(),
            note_status = $("input:radio[name=note_status]:checked").val();
        
		if (note_id != '' && note_type != '' && note_text != '' && note_start != '' && note_end != '' && note_status != '') {showLoader();
            $.ajax({
                cache: false,
                type: 'POST',
                url: '<?php echo site_url()?>/monitor/update_notification',
                data: {id:note_id, type:note_type, notification:note_text, start:note_start, end:note_end, status:note_status},
                success: function(data) {
                                if (data == 1) {
                                    hideLoader();
                                    $('#dialog_notes').html('<b>Updated Successfully</b>');
                                    $('#dialog_notes').dialog('open');
                                }
                                else {
                                    hideLoader();
                                    $('#dialog_notes').html('<b>Updated Failed</b>');
                                    $('#dialog_notes').dialog('open');
                                }
                            }
            });
		}else{
                $('#dialog_notes').html('<b>Please check your input</b>');
                $('#dialog_notes').dialog('open');
            }
	}
    
    $(document).ready(function() {
        $( "#enote_start" ).datepicker({
            dateFormat: "yy-mm-dd",
            firstDay: 1 ,
			minDate: 0,
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function( selectedDate ) {
                            if ( selectedDate ) {
                                $( "#enote_end" ).datepicker( "option", "minDate", selectedDate );
                            }
                        }
        });
        
        $( "#enote_end" ).datepicker({
            dateFormat: "yy-mm-dd",
            firstDay: 1 ,
            minDate: 0,
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function( selectedDate ) {
                                $( "#enote_start" ).datepicker( "option", "maxDate", selectedDate );
                        }
        });
    });
</script>
