<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Header( "Last-Modified: " . gmdate( "D, j M Y H:i:s" ) . " GMT" );
Header( "Expires: " . gmdate( "D, j M Y H:i:s", time() ) . " GMT" );
Header( "Cache-Control: no-store, no-cache, must-revalidate" ); // HTTP/1.1
Header( "Cache-Control: post-check=0, pre-check=0", FALSE );
Header( "Pragma: no-cache" ); // HTTP/1.0

date_default_timezone_set('Asia/Jakarta');	
 ?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
	<title>Media Insight Agent</title>
	<?php echo $extraHeadContent; ?>	
</head>
<body>
	<div id="roundedind">
		<div id="header" style="margin-bottom:30px;">
			<img src="<?php echo base_url()?>asset/images/<?php echo $this->session->userdata('logo_head')?>" style="max-height:80px;margin-top:10px; position:relative;">
			<div id="menuhead">
				<div id="menub" style="background-color:<?php echo $this->session->userdata('col_theme')?>;">
					<?php $this->load->view('main/menu'); ?>
				</div>
				<div id="date">
					&nbsp;&nbsp;
					<?php echo date('l, jS \of F Y'); ?>
				</div>
				<div id="flage">
				<!--
					<a href="javascript:;" title="indonesia" onclick="change_lang('ind')">
						<img src="<?php echo base_url()?>asset/images/ind.png" />
					</a>
					&nbsp;
					<a href="javascript:;" title="english" onclick="change_lang('eng')">
						<img src="<?php echo base_url()?>asset/images/eng.png" />
					</a>
				-->
				</div>
			</div>
		</div>
		<div id="blocke"></div>

<script>
<?php
	$getserv = $_SERVER['QUERY_STRING'] ? '?'.$_SERVER['QUERY_STRING'] : '';
	$curpage = $this->config->site_url()."/".$this->uri->uri_string().$getserv; 
?>
	function change_lang(id)
	{
		var uri = '<?php echo $curpage?>';
		if(id!='<?php echo $this->session->userdata('lang')?>')
			{
				window.location.href = '<?php echo site_url()."/home/change_lang"?>?id='+id+'&uri='+uri+'';
			}
	}
	function showLoader()
	{
        jQuery('#blocke').fadeIn(200);
    }
    function hideLoader()
	{
		jQuery('#blocke').fadeOut(200);
    }
	
	function originale(pdfe, stat)
	{
		if(pdfe!='' && stat!='')
		{
			/*
			var form = document.createElement("form");
				form.setAttribute("method", "post");
				form.setAttribute("action", "<?php echo site_url()?>/news/get_original");

				var hiddenField1 = document.createElement("input");					
				hiddenField1.setAttribute("name", "pdfe");
				hiddenField1.setAttribute("id", "pdfe");
				hiddenField1.setAttribute("value", pdfe);
				var hiddenField2 = document.createElement("input");		
				hiddenField2.setAttribute("name", "stat");
				hiddenField2.setAttribute("id", "stat");
				hiddenField2.setAttribute("value", stat);
				
				form.appendChild(hiddenField1);
				form.appendChild(hiddenField2);
				document.body.appendChild(form);	
				form.submit();
				$("#pdfe").remove();
				$("#stat").remove();
			*/
			$.ajax({
										async: false,
										type:'POST',
										url:'<?php echo site_url()?>/news/get_original',
										data:{'pdfe':pdfe, 'stat':stat},
										success:function(data) {
											window.open(data, '_blank');
										}
								});
		
		}
	}
</script>