<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');?>

<div id="panelkiri">
	<div id="user"><b><?php echo $this->lang->line('current_user')?>:</b></div>
	<div id="user2"><?php echo $this->session->userdata('usr_agent_name');?></div>
	<div id="paneldalam">
		<div id="leftbar">
		   <div id="pilihtanggal">
				<input type="hidden" id="timeframe" value="7" />
				<div id="kotakpilihtgl">
				  <p class="title"><?php echo $this->lang->line('any_time')?></p></div>
				<a href="javascript:;" onclick="change_frame(1)">
				<div id="kotakpilihtgl2">
					<p class="title2"><?php echo $this->lang->line('past_day')?></p>
				</div>
				</a>
				<a href="javascript:;" onclick="change_frame(7)" >
				<div id="kotakpilihtgl2">
					<p class="title2"><?php echo $this->lang->line('past_week')?></p>
				</div>
				</a>
				<a href="javascript:;" onclick="change_frame(30)">
				<div id="kotakpilihtgl2">
					<p class="title2"><?php echo $this->lang->line('past_month')?></p>
				</div>
				</a>
				<a href="javascript:;" onclick="change_frame(365)">
				<div id="kotakpilihtgl2">
					<p class="title2"><?php echo $this->lang->line('past_year')?></p>
				</div>
				</a>  
				<div id="kotakpilihtgl">
				<a href="javascript:;" onclick="show_date_range()">
					<p class="title">
					<?php echo $this->lang->line('select_range')?></p>
				</a>
				</div>
				<span id="antartgl">
				<p>&nbsp;</p>
				<ul>
					<li><label for="from">&nbsp;&nbsp;<?php echo $this->lang->line('date')?> :</label><br/><input type="text" id="from" name="from"/></li>
					<li><label for="to">&nbsp;&nbsp;<?php echo $this->lang->line('date_to')?>:</label><br/><input type="text" id="to" name="to"/></li>
					<a href="javascript:;" onclick="get_result_date()">
						<div id="kotakpilihtgl2"><p class="title"><?php echo $this->lang->line('search')?></p></div>
					</a>
				</ul>
				</span>    
			</div>  
		</div>		
	</div>
</div>

<div id="blocke" align="center">
	<p><b>Processing...</b></p>
	<p><img src="<?php echo base_url()?>asset/images/loads.gif"></p>
</div>

<script>
//------------------------Standar leftpanel------------------------------
	$(document).ready(function(){
		$('#antartgl').hide();
		$("#from").val('');
		$("#to").val('');	
		
		$( "#from, #to" ).datepicker({
			dateFormat: "yy-mm-dd" ,
			maxDate:0
		});
		
	});
	
	function get_result_date()
	{
		var from      = $("#from").val();
		var to        = $("#to").val();
		if(from!='' && to!='')
		{
			get_result();
		}
	}
	
	function change_frame(vale)
	{
		var timeframe = $("#timeframe").val(vale);
		if(timeframe!='')
		{
			get_result();
		}
	}
	
	function show_date_range()
	{
		if ($('#antartgl').is(':visible'))
		{
			$('#antartgl').hide("slow");
			$("#from").val('');
			$("#to").val('');			
		}
		else
		{
			$('#antartgl').show("slow");
		}
	}

//-----------------------------------------------------------------------------	
	
</script>