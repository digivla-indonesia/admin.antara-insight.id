<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$this->load->view('main/header');
$this->load->view($content);
$this->load->view('main/footer');
?>