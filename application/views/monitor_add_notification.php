<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div id="tabs1">
	<div class="CSSTableGenerator" style="width:80%">
		<form method="post" action="" id="reg_notification">
            <table>
                <tr><td colspan=3>Add Notification</td></tr>
                <tr>
                    <td width="20%">Company<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%">
                        <select id="company" style="width:500px;height:20px;color:black;margin:0;">
                            <option value="">Choose Company</option>
                    <?php foreach($comp->result() as $co) { ?>
                            <option value="<?php echo $co->comp_id.'#@$%'.$co->client_id;?>"><?php echo $co->comp_name."&nbsp;(".$co->comp_status.")";?></option>
                    <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Type<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%">
                        <select id="note_type" style="width:200px;height:20px;color:black;margin:0;">
                            <option value="">Choose Type</option>
                            <option value="info">Information</option>
                            <option value="warn">Warning</option>
                            <option value="alrt">Alert</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Notification<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%"><input type="text" style="width:500px;height:25px;" id="notification"></td>
                </tr>
                <tr>
                    <td width="20%">Period<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%">
                        <input type="text" style="width:100px;height:25px;" id="note_start"/>
                        &nbsp;&nbsp;to&nbsp;&nbsp;
                        <input type="text" style="width:100px;height:25px;" id="note_end"/>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Status<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%">
                        <input type="radio" name="note_status" id="nstatusA" value="A" CHECKED />&nbsp;&nbsp;Active<br>
                        <input type="radio" name="note_status" id="nstatusI" value="I" />&nbsp;&nbsp;Inactive<br>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="right">
                        <input type="button" class="ui-button" value="<?php echo $this->lang->line('save')?>" id="fsubmit" onClick="save_notification()" />
                        <input type="reset" class="ui-button" value="<?php echo $this->lang->line('reset')?>" />&nbsp;&nbsp;&nbsp;
                    </td>
                </tr>
            </table>
		</form>
	</div>
</div>

<script>
	function save_notification() {
		var company_tag = $("#company").val(),
            note_type   = $("#note_type").val(),
            note_text   = $("#notification").val(),
            note_start  = $("#note_start").val(),
            note_end    = $("#note_end").val(),
            note_status = $("input:radio[name=note_status]:checked").val();
        
		if (company_tag != '' && note_type != '' && note_text != '' && note_start != '' && note_end != '' && note_status != '') {
            showLoader();
            $.ajax({
                cache: false,
                type: 'POST',
                url: '<?php echo site_url()?>/monitor/add_notification',
                data: {company:company_tag, type:note_type, notification:note_text, start:note_start, end:note_end, status:note_status},
                success: function(data) {
                                if (data == 1) {
                                    hideLoader();
                                    $('#dialog_notes').html('<b>Saved Successfully</b>');
                                    $('#dialog_notes').dialog('open');
                                    $('#reg_notification').resetForm();
                                }
                                else {
                                    hideLoader();
                                    $('#dialog_notes').html('<b>Saved Failed</b>');
                                    $('#dialog_notes').dialog('open');
                                }
                            }
            });
		}else{
                $('#dialog_notes').html('<b>Please check your input</b>');
                $('#dialog_notes').dialog('open');
            }
	}
    
    $(document).ready(function() {
        $( "#note_start" ).datepicker({
            dateFormat: "yy-mm-dd",
            firstDay: 1 ,
			minDate: 0,
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function( selectedDate ) {
                            if ( selectedDate ) {
                                $( "#note_end" ).datepicker( "option", "minDate", selectedDate );
                            }
                        }
        });
        $( "#note_end" ).datepicker({
            dateFormat: "yy-mm-dd",
            firstDay: 1 ,
            minDate: 0,
            changeMonth: true,
            numberOfMonths: 1,
            onClose: function( selectedDate ) {
                                $( "#note_start" ).datepicker( "option", "maxDate", selectedDate );
                        }
        });
    });
</script>
