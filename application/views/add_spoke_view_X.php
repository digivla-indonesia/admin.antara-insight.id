<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div id="tabs1">
	<div id="blockeS" align="center">
		<p><b>Processing...</b></p>
		<p><img src="<?php echo base_url()?>asset/images/loads.gif"></p>
	</div>
	<div class="CSSTableGenerator" style="width:80%" >
		<form id="form_spoke">
		<table>
			<tr><td colspan=3>Register Spokeperson in Quotes</td></tr>
			<tr>
				<td width="30%"><?php echo $this->lang->line('user_fullname')?><font color="red">&nbsp;&nbsp;*</font></td><td width="5%" align="center">:</td>
				<td width="50%"><input type="text" style="width:500px;height:25px;" name="fullname" id="fullname" /></td>
			</tr>
			<tr>
				<td width="30%">Upload Foto<font color="red">&nbsp;&nbsp;*<br>(Must .jpg file)</font></td><td width="5%" align="center">:</td>
				<td width="50%"><input type="file" style="width:400px;" name="compy_logo" id="compye_logo"/></td>
			</tr>
			<tr>
				<td>&nbsp;</td><td>&nbsp;</td>
			</tr>
			<tr>
				<td width="30%">Searching Name<font color="red">&nbsp;&nbsp;*</font><br>(Enter to show result)</td><td width="5%" align="center">:</td>
				<td width="50%"><input type="text" style="width:500px;height:25px;" id="cariSpoker"/></td>
			</tr>	
			<tr>
				<td>&nbsp;</td><td></td><td><div id="spokeperson_temp"></div></td>
			</tr>
			<tr>
				<td>&nbsp;</td><td>&nbsp;</td>
				<td align="right">
					<input type="reset" class="ui-button" value="<?php echo $this->lang->line('reset')?>" />&nbsp;&nbsp;&nbsp;
					<input type="button" class="ui-button" value="<?php echo $this->lang->line('save')?>" onclick="save_spoke()" />
				</td>
			</tr>
		</table>
		</form>
	</div>
</div>

<script>
	$('#cariSpoker').keypress(function(event) {
		var huruf = $('#cariSpoker').val();
		var start = 0;
		var limit = 100;
		if (event.keyCode == 13 && huruf!='') {
			showLoaderS();
			$.ajax({
					cache:false,
					type:'POST',
					url:'<?php echo site_url()?>/spokeperson/get_spokepersonByInitial',
					data:{'initial':huruf,'start':start,'limit':limit },
					success:function(data) { 
						$("#spokeperson_temp").html('');
						$("#spokeperson_temp").html(data);
						hideLoaderS();
					}
			});
		}
	});
	
	function showLoaderS()
	{
        jQuery('#blockeS').fadeIn(200);
    }
    function hideLoaderS()
	{
		jQuery('#blockeS').fadeOut(200);
    }
	
	function check_all(parame)
	{
		if(parame==0)
		{
			$('#medset_table .medsetdach').prop('checked', true);
		}
		else
		{
			$('#medset_table .medsetdach').prop('checked', false);
		}
	}
	
	function save_spoke()
	{
		var isi = [];
		$("input[id='medset_detail']:checked").each(function (){
            isi.push((jQuery(this).val()));
        });
		
		var fullname = $("#fullname").val();
		//alert(isi);
		if( fullname!='' && isi!='')
		{			
			showLoader();
			$("#form_spoke").ajaxSubmit({
			cache:false,
			type:'POST',
			url:'<?php echo site_url()?>/spokeperson/save_spokerperson',
			data:{'isi':isi, 'edit':''},
			success:function(data) {
					if(data==1)
					{
						hideLoader();
						$('#dialog_warn').html('<b>Saved Successfully</b>');
						$('#dialog_warn').dialog('open');
					}
					
					else if(data==4)
					{
						hideLoader();
						$('#dialog_warn').html('<b>Username Already Used</b>');
						$('#dialog_warn').dialog('open');
					}
					else
					{
						hideLoader();
						$('#dialog_warn').html('<b>Saved Failed</b>');
						$('#dialog_warn').dialog('open');
					}
				}
			});
		}
		else
		{
			$('#dialog_warn').html('<b>Check Your Input</b>');
			$('#dialog_warn').dialog('open');
		}

	}
	
</script>
