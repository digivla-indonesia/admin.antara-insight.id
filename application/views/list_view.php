<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div align="center">
    <div id="tabs" style="width:96%;min-height:650px;">
        <ul>
            <li><a href="#tabs1"><?php echo $this->lang->line('list_comp')?></a></li>
            <li><a href="<?php echo site_url()?>/liste/liste_user">User List</a></li>
            <li><a href="<?php echo site_url()?>/liste/liste_newsletter">Newsletter</a></li>
        </ul>
        <div id="tabs1">
            <div id="data_comp">
                <table id="list_comp"></table>
                <div id="comp_pager"></div>
            </div>
        </div>
    </div>
</div>

<!-- dialog -->
<div id="dialog_list"></div>
<div id="dialog_media"></div>
<div id="dialog_warn" align="center"></div>
<div id="dialog_delete"></div>

<script>
	$('#tabs').tabs();
	
	$(document).ready(function() {
		jQuery("#list_comp").jqGrid({
            url: '<?php echo site_url()?>/liste/get_list_company',
            mtype: "post",
            datatype: "json",
            colNames: ['No', 'Company Name', 'DPU', 'Expired', 'Status', 'Profile', 'Media', 'Delete'],
            colModel: [
                        {name:'no', index:'no', width:12, align:"center", search:false, sortable:false, resizable:false},
                        {name:'comp_name', index:'comp_name', search:false},
                        {name:'comp_dpu', index:'comp_dpu', width:40, align:"right", search:false, sortable:false, resizable:false},
                        {name:'comp_expired', index:'comp_expired', width:32, align:"center", search:false},
                        {name:'comp_status', index:'comp_status', width:24, search:false},
                        {name:'profile', index:'profile', width:14, align:"center", search:false, sortable:false, resizable:false},
                        {name:'media', index:'media', width:14, align:"center", search:false, sortable:false, resizable:false},
                        {name:'delete', index:'delete', width:20, align:"center", search:false, sortable:false, resizable:false}
                    ],
            rowNum: 25,
            width: 875,
            height: 462,
            rowList: [25,50,100],
            pager: '#comp_pager',
            sortname: 'comp_id',
            viewrecords: true,
            //multiselect: true,
            caption:"Company List"
		}).jqGrid('navGrid', '#comp_pager',
                    {edit:false, add:false, del:false}, //options
                    {}, // add options
                    {}, // del options
                    {overlay:false}, // edit options
                    //	{sopt: ['bw','eq','ne','lt','le','gt','ge','ew','cn']} // search options
                    {sopt: ['cn']} // search options
                );
        // akhir setting jqgrid =====================================================	
		
        $(document).tooltip();
	
        $("#dialog_list").dialog({
                modal: true,
                height: 'auto',
                width: '700px',
                autoOpen: false,
                iframe: true,
                open: $(this).parent().appendTo($("form:first")),
                position: 'top',
                title: 'Detail Company',
                buttons: {		
                            'Change': function() {
                                            update_company();
                                        },
                            'Close': function() {
                                            $(this).dialog('close');
                                            $('#list_comp').trigger("reloadGrid");
                                        }
                    }
        });
        
        $("#dialog_warn").dialog({
				height: 'auto',
				width: '400px',
				autoOpen: false,
				modal: true,
				position:'middle',
				title: 'Confirmation',
				buttons: {		
                            'Close': function() {
                                            $(this).dialog('close');
                                        }
                    }
		});
	
        $("#dialog_media").dialog({
				height: 'auto',
				width: '650px',
				autoOpen: false,
				modal: true,
				position:'top',
				title: 'Detail Media',
				open: function() {
                            $(".accordion").accordion({ autoHeight: false,collapsible: true });
                        },
				buttons: {		
                            'Save': function() {
                                            save_media();
                                        },
                            'Close': function() {
                                            $(this).dialog('close');
                                            $('#list_comp').trigger("reloadGrid");
                                        }
                    }
        });
        
        $("#dialog_delete").dialog({
				height: 'auto',
				width: '500px',
				autoOpen: false,
				modal: true,
				position:'middle',
				title: 'Delete Company',
        });
	});
	
	function detail_company(id) 
	{
		$.ajax({
            cache: false,
            type: 'POST',
            url: '<?php echo site_url()?>/liste/detail_company',
            data: {'comp_id':id},
            success: function(data) {
                            $('#dialog_list').html(data);
                            $('#dialog_list').dialog('open');
                        }
		});
	}
	
	function detail_media(id) 
	{
		$.ajax({
            cache: false,
            type: 'POST',
            url: '<?php echo site_url()?>/liste/detail_media',
            data: {'comp_id':id},
            success: function(data) {
                            $('#dialog_media').html(data);
                            $('#dialog_media').dialog('open');
                        }
		});
		
	}
	function delete_user(id, nama, client_id) 
	{
		$('#dialog_delete').dialog({
			buttons: {
                        'Delete': function() {
                                        if (id!='')
                                        {
                                        	return alert("ntar dihapus alertnya ya");
                                            $.ajax({
                                                cache: false,
                                                type: 'POST',
                                                url: '<?php echo site_url()?>/liste/delete_company',
                                                data: {'comp_id':id, 'client_id': client_id},
                                                success: function(data) {
                                                                if (data==1)
                                                                {
                                                                    $('#dialog_warn').html('<b>Saved Successfully</b>');
                                                                    $('#dialog_warn').dialog('open');
                                                                    $('#list_user').trigger("reloadGrid");
                                                                }
                                                                else
                                                                {
                                                                    $('#dialog_warn').html('<b>Saved Failed</b>');
                                                                    $('#dialog_warn').dialog('open');
                                                                    $('#list_user').trigger("reloadGrid");
                                                                }
                                                            }
                                            });
                                        }
                                        $(this).dialog('close');
                                    },
                        'Cancel': function() {
                                        $(this).dialog('close');
                                    }
                }
		});
		$('#dialog_delete').html('<b>Are You Sure Delete This User ? <br><font color=red>'+nama+'</font></b>');
		$('#dialog_delete').dialog('open');
	}
</script>
