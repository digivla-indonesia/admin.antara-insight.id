<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div id="tabs1">
    <div id="data_notes">
        <table id="list_note"></table>
        <div id="note_pager"></div>
    </div>
</div>

<!-- dialog -->
<div id="dialog_nt_view"></div>
<div id="dialog_nt_delete"></div>
<div id="dialog_notes"></div>

<script>
	$('#tabs').tabs();
	
	$(document).ready(function() {
		jQuery("#list_note").jqGrid({
            url: '<?php echo site_url()?>/monitor/get_list_notification',
            mtype: "post",
            datatype: "json",
            colNames: ['No', 'Company Name', 'Notification', 'Period', 'Status', 'View', 'Delete'],
            colModel: [
                        {name:'no', index:'no', width:24, align:"center", search:false, sortable:false, resizable:false},
                        {name:'comp_name', index:'comp_name', width:100, search:false},
                        {name:'notification', index:'notify_type', search:false, resizable:false},
                        {name:'period', index:'start_date', width:84, align:"center", search:false},
                        {name:'status', index:'notify_status', width:36, search:false},
                        {name:'view', index:'view', width:24, align:"center", search:false, sortable:false, resizable:false},
                        {name:'delete', index:'delete', width:24, align:"center", search:false, sortable:false, resizable:false}
                    ],
            rowNum: 25,
            width: 875,
            height: 462,
            rowList: [25,50,100],
            pager: '#note_pager',
            sortname: 'start_date',
            viewrecords: true,
            //multiselect: true,
            caption:"Notification List"
		}).jqGrid('navGrid', '#note_pager',
                    {edit:false, add:false, del:false}, //options
                    {}, // add options
                    {}, // del options
                    {overlay:false}, // edit options
                    //	{sopt: ['bw','eq','ne','lt','le','gt','ge','ew','cn']} // search options
                    {sopt: ['cn']} // search options
                );
        // akhir setting jqgrid =====================================================	
		
        $(document).tooltip();
	
        $("#dialog_nt_view").dialog({
				height: 'auto',
				width: '700px',
				autoOpen: false,
				modal: true,
				position:'top',
				title: 'Detail Notification',
				buttons: {		
                            'Change': function() {
                                            update_notification();
                                        },
                            'Close': function() {
                                            $(this).dialog('close');
                                            $('#list_note').trigger("reloadGrid");
                                        }
                        }
        });
        
        $("#dialog_nt_delete").dialog({
				height: 'auto',
				width: '500px',
				autoOpen: false,
				modal: true,
				position:'middle',
				title: 'Delete Notification',
        });
        
        $("#dialog_notes").dialog({
				height: 'auto',
				width: '400px',
				autoOpen: false,
				modal: true,
				position: 'middle',
				title: 'Confirmation',
				buttons: {
                            'Close': function() {
                                            $(this).dialog('close');
                                        }
                    }
		});
	});
	
	function detail_note(id) {
		$.ajax({
            cache: false,
            type: 'POST',
            url: '<?php echo site_url()?>/monitor/detail_notification',
            data: {id:id},
            success: function(data) {
                            $('#dialog_nt_view').html(data);
                            $('#dialog_nt_view').dialog('open');
                        }
		});
	}
	
	function delete_note(id, detail) {
		$('#dialog_nt_delete').dialog({
			buttons: {
                        'Delete': function() {
                                        if (id != '') {
                                            $.ajax({
                                                cache: false,
                                                type: 'POST',
                                                url: '<?php echo site_url()?>/monitor/delete_notification',
                                                data: {'id':id},
                                                success: function(data) {
                                                                if (data == 1) {
                                                                    $('#dialog_notes').html('<b>Delete Successfully</b>');
                                                                    $('#dialog_notes').dialog('open');
                                                                    $('#list_note').trigger("reloadGrid");
                                                                }else{
                                                                    $('#dialog_notes').html('<b>Delete Failed</b>');
                                                                    $('#dialog_notes').dialog('open');
                                                                    $('#list_note').trigger("reloadGrid");
                                                                }
                                                            }
                                            });
                                        }
                                        $(this).dialog('close');
                                    },
                        'Cancel': function() {
                                        $(this).dialog('close');
                                    }
                }
		});
		$('#dialog_nt_delete').html('<b>Are You Sure Delete This Notification? <br><font color=red>'+detail+'</font></b>');
		$('#dialog_nt_delete').dialog('open');
	}
</script>
