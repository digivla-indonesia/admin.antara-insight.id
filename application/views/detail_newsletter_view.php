<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

    <div class="CSSTableGenerator" style="width:100%">
		<form method="post" action="" id="upd_newsletter">
            <input type="hidden" id="nl_id" value="<?=$alert_id;?>"/>
            <table>
                <tr><td colspan=3>&nbsp;</td></tr>
                <tr>
                    <td width="20%">Company<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%"><?=$company_name;?></td>
                </tr>
                <tr>
                    <td width="20%">Recipient<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%">
                        <textarea style="width:500px;" id="recipient"><?=$recipient_list;?></textarea>
                        <p style="color:#666666;font-style:italic;">*please use semicolon (<b> ; </b>) to separate email address.</p>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Category<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%"><ul id="catlist"><?=$category_list;?></ul></td>
                </tr>
                <tr>
                    <td width="20%">Start Date<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%"><input type="text" style="width:100px;height:25px;" id="start_nwslttr" value="<?=$start_date;?>"/></td>
                </tr>
                <tr>
                    <td width="20%">Recurrence<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%">
                        <select id="recurrence" style="width:200px;height:20px;color:black;margin:0;">
                            <option value="">Choose Recurrence</option>
                        <?php foreach($reps->result() as $rep) { ?>
                            <option value="<?=$rep->time_id;?>" <?=($rep->time_id == $recc) ? 'SELECTED' : '';?>><?=$rep->time_repeat;?></option>
                        <?php }?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Status<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%">
                        <input type="radio" name="nlstatus" id="nlstatusA" value="A" <?=($status == 'A') ? 'CHECKED' : '';?>/>&nbsp;&nbsp;Active<br>
                        <input type="radio" name="nlstatus" id="nlstatusI" value="I" <?=($status == 'I') ? 'CHECKED' : '';?>/>&nbsp;&nbsp;Inactive<br>
                    </td>
                </tr>
            </table>
		</form>
	</div>
    
    <script>
        function update_newsletter() {
            var nl_id       = $("#nl_id").val(),
                recipients  = $("#recipient").val(),
                nl_start    = $("#start_nwslttr").val(),
                nl_repeat   = $("#recurrence").val(),
                nl_status   = $("input:radio[name=nlstatus]:checked").val(),
                cat_set     = '',
                cat_id      = '';
		
            $('input[name="catlist[]"]:checked').each(function() {
                var chkval = $(this).val(),
                    chktag = $(this).data('tag');
                
                if (chkval != '') { cat_set += chkval+';'; }
                if (chktag != '') { cat_id  += chktag; }
            });
            
            if (nl_id != '' && recipients != '' && nl_start != '' && nl_repeat != '' && nl_status != '' && cat_set != '' && cat_id != '') {
                showLoader();
                $.ajax({
                    cache: false,
                    type: 'POST',
                    url: '<?php echo site_url()?>/liste/update_newsletter',
                    data: {id:nl_id, recipient:recipients, start:nl_start, repeat:nl_repeat, status:nl_status, catset:cat_set, catid:cat_id},
                    success: function(data) {
                                    if (data == 1) {
                                        hideLoader();
                                        $('#dialog_warn').html('<b>Updated Successfully</b>');
                                        $('#dialog_warn').dialog('open');
                                    }
                                    else {
                                        hideLoader();
                                        $('#dialog_warn').html('<b>Update Failed</b>');
                                        $('#dialog_warn').dialog('open');
                                    }
                                }
                });
            }else{
                    $('#dialog_warn').html('<b>Please check your input</b>');
                    $('#dialog_warn').dialog('open');
                }
        }
        
        $(document).ready(function() {
            $( "#start_nwslttr" ).datepicker({
                dateFormat: "yy-mm-dd" ,
                firstDay: 1 ,
                minDate:0
            });
        });
    </script>