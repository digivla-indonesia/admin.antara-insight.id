<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div id="tabs1">
	<div class="CSSTableGenerator" style="width:80%">
		<form id="form_user_reg">
            <table>
                <tr><td colspan=3>Register User</td></tr>
                <tr>
                    <td width="20%"><?php echo $this->lang->line('user_fullname')?><font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%"><input type="text" style="width:500px;height:25px;" id="fullname"/></td>
                </tr>
                <tr>
                    <td width="20%"><?php echo $this->lang->line('email')?><font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%"><input type="text" style="width:500px;height:25px;" id="email"/></td>
                </tr>
                <tr>
                    <td width="20%"><?php echo $this->lang->line('username')?><font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%"><input type="text" style="width:500px;height:25px;" id="username"/></td>
                </tr>
                <tr>
                    <td width="20%"><?php echo $this->lang->line('password')?><font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%"><input type="password" style="width:500px;height:25px;" id="password1"/></td>
                </tr>
                <tr>
                    <td width="20%"><?php echo $this->lang->line('confirm_pass')?><font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%"><input type="password" style="width:500px;height:25px;" id="password2"/></td>
                </tr>
                <tr>
                    <td width="20%"><?php echo $this->lang->line('company')?><font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%">
                        <select id="company" style="width:300px;height:20px;color:black;">
                            <option value="">Choose Company</option>
                        <?php foreach($comp->result() as $co)
                        {?>
                            <option value="<?php echo $co->comp_id.'#@$%'.$co->client_id;?>"><?php echo $co->comp_name."&nbsp;(".$co->comp_status.")";?></option>
                        <?php
                        }?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="20%"><?php echo $this->lang->line('acess_menu')?><font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%">
                    <?php foreach($levele->result() as $lev){?>
                        <input type="radio" name="acess" id="acess<?php echo $lev->level_id?>" value="<?php echo $lev->level_id?>"  onchange="get_acess()" />&nbsp;&nbsp;<?php echo $lev->level_name?>&nbsp;<?php echo $lev->level_desc?><br>
                    <?php }?>
                        <div id="custom" style="margin-left:20px;">
                            Select Menu :
                            <ul id="custom_menu">
                                <?php foreach($menune->result() as $me){?>
                                    <li><input class="menu" type='checkbox' brand='menu_item[]' value='<?php echo $me->menu_id;?>'><?php echo $me->menu_name;?></li>
                                <?php }?>
                                    <li><input type="button" onclick="all_custom()" value="ALL"></li>
                            </ul>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="right">
                        <input type="reset" class="ui-button" value="<?php echo $this->lang->line('reset')?>" />&nbsp;&nbsp;&nbsp;
                        <input type="button" class="ui-button" value="<?php echo $this->lang->line('save')?>" onclick="save_user()" />
                    </td>
                </tr>
            </table>
		</form>
	</div>
</div>

<script>
    $('#custom').hide();
    
	function get_acess()
	{
		if ($('#acess4').is(':checked')==true)
		{
			$('#custom').show("slow");
		}
		else
		{
			$('#custom').hide("slow");
		}
	}
    
	function all_custom()
	{
		$('#custom_menu .menu').prop('checked', true);
	}
    
	function save_user()
	{
		var fullname    = $("#fullname").val();
		var email       = $("#email").val();
		var username    = $("#username").val();
		var password1   = $("#password1").val();
		var password2   = $("#password2").val();
		var company     = $("#company").val();
		var cek_acess   = $("input:radio[name=acess]:checked").val();
		var isi         = '';
//console.log('fullname: ' + fullname + ', email: ' + email + ', username: ' + username + ', password: ' + password1 + ', company: ' + company + ', acess: ' + isi);
		if (cek_acess==4)
        {
            isi = [];
            $("input[brand='menu_item[]']:checked").each(function () {
                isi.push((jQuery(this).val()));
            });
        }
		else
			{
				isi = cek_acess;
			}

		if (company!='' && username!=''&& fullname!=''&& email!=''&& password1!='' && password2!='' && $("input:radio[name=acess]").is(':checked') && isi!='')
		{
			if (password1==password2)
			{
				showLoader();
				$.ajax({
                    cache: false,
                    type: 'POST',
                    url: '<?php echo site_url()?>/register/register_user',
                    data: {'fullname':fullname, 'email':email, 'username':username, 'password':password1, 'company':company,'acess':isi},
                    success: function(data) {
                                    if (data==1)
                                    {
                                        hideLoader();
                                        $('#dialog_sucess_user').html('<b>Saved Successfully</b>');
                                        $('#dialog_sucess_user').dialog('open');
                                    }
                                    else if (data==4)
                                    {
                                        hideLoader();
                                        $('#dialog_warn_user').html('<b>Username Already Used</b>');
                                        $('#dialog_warn_user').dialog('open');
                                    }
                                    else if (data==0)
                                    {
                                        hideLoader();
                                        $('#dialog_warn_user').html('<b>Email Not Valid</b>');
                                        $('#dialog_warn_user').dialog('open');
                                    }
                                    else
                                    {
                                        hideLoader();
                                        //$('#dialog_warn_user').html('<b>Saved Failed</b>');
                                        $('#dialog_warn_user').html(JSON.stringify(data));
					$('#dialog_warn_user').dialog('open');
                                    }
                                }
				});
			}
			else
			{
				$('#dialog_warn_user').html('<b>The password and confirm password field do not match</b>');
				$('#dialog_warn_user').dialog('open');
			}
		}
		else
		{
			$('#dialog_warn_user').html('<b>Check Your Input</b>');
			$('#dialog_warn_user').dialog('open');
		}
	}
</script>
