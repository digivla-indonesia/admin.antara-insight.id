<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div id="tabs1">
	<div class="CSSTableGenerator" style="width:80%">
		<form method="post" action="" id="reg_newsletter">
            <table>
                <tr><td colspan=3>Register Newsletter</td></tr>
                <tr>
                    <td width="20%">Company<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%">
                        <select id="company" style="width:500px;height:20px;color:black;margin:0;" onChange="get_category()">
                            <option value="">Choose Company</option>
                        <?php foreach($comp->result() as $co)
                        {?>
                            <option value="<?php echo $co->comp_id.'#@$%'.$co->client_id;?>"><?php echo $co->comp_name."&nbsp;(".$co->comp_status.")";?></option>
                        <?php
                        }?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Recipient<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%">
                        <textarea style="width:500px;" id="recipient"></textarea>
                        <p style="color:#666666;font-style:italic;">*please use semicolon (<b> ; </b>) to separate email address.</p>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Category<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%"><ul id="catlist"><li>*** Empty list (choose company first) ***</li></ul></td>
                </tr>
                <tr>
                    <td width="20%">Start Date<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%"><input type="text" style="width:100px;height:25px;" id="start_nwslttr"/></td>
                </tr>
                <tr>
                    <td width="20%">Recurrence<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%">
                        <select id="recurrence" style="width:200px;height:20px;color:black;margin:0;">
                            <option value="">Choose Recurrence</option>
                        <?php foreach($reps->result() as $rep) { ?>
                            <option value="<?=$rep->time_id;?>"><?=$rep->time_repeat;?></option>
                        <?php }?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td width="20%">Status<font color="red">&nbsp;&nbsp;*</font></td>
                    <td width="2%" align="center">:</td>
                    <td width="78%">
                        <input type="radio" name="nlstatus" id="nlstatusA" value="A" CHECKED />&nbsp;&nbsp;Active<br>
                        <input type="radio" name="nlstatus" id="nlstatusI" value="I" />&nbsp;&nbsp;Inactive<br>
                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td align="right">
                        <input type="reset" class="ui-button" value="<?php echo $this->lang->line('reset')?>" />&nbsp;&nbsp;&nbsp;
                        <input type="button" class="ui-button" value="<?php echo $this->lang->line('save')?>" id="fsubmit" onClick="save_newsletter()" />
                    </td>
                </tr>
            </table>
		</form>
	</div>
</div>

<script>
    function get_category() {
        var compid = $('#company').val();
        
        $('#catlist').html('<li><i>Searching...</i></li>');
        if (compid!='') {
            $.ajax({
                cache: false,
                type: 'POST',
                url: '<?php echo site_url()?>/register/get_category_list',
                data: {compid:compid},
                success: function(data) {
                                $('#catlist').html(data);
                            }
            });
        }else{
                $('#catlist').html('<li>*** Empty list (choose company first) ***</li>');
            }
    }
    
	function save_newsletter() {
		var company_tag = $("#company").val(),
            recipients  = $("#recipient").val(),
            nl_start    = $("#start_nwslttr").val(),
            nl_repeat   = $("#recurrence").val(),
            nl_status   = $("input:radio[name=nlstatus]:checked").val(),
            cat_set     = '',
            cat_id      = '';
		
        $('input[name="catlist[]"]:checked').each(function() {
            var chkval = $(this).val(),
                chktag = $(this).data('tag');
            
            if (chkval != '') { cat_set += chkval+';'; }
            if (chktag != '') { cat_id  += chktag; }
        });
        
		if (company_tag != '' && recipients != '' && nl_start != '' && nl_repeat != '' && nl_status != '' && cat_set != '' && cat_id != '') {
            //var test = 'company tag >> '+company_tag+'\n'
            //            +'email >> '+recipients+'\n'
            //            +'start >> '+nl_start+'\n'
            //            +'repeat >> '+nl_repeat+'\n'
            //            +'status >> '+nl_status+'\n'
            //            +'set ID >> '+cat_set+'\n'
            //            +'keywords >> '+cat_id;
            //alert(test);
            
            showLoader();
            $.ajax({
                cache: false,
                type: 'POST',
                url: '<?php echo site_url()?>/register/register_newsletter',
                data: {company:company_tag, recipient:recipients, start:nl_start, repeat:nl_repeat, status:nl_status, catset:cat_set, catid:cat_id},
                success: function(data) {
                                if (data == 1) {
                                    hideLoader();
                                    $('#dialog_sucess_user').html('<b>Saved Successfully</b>');
                                    $('#dialog_sucess_user').dialog('open');
                                }
                                else {
                                    hideLoader();
                                    $('#dialog_warn_user').html('<b>Saved Failed</b>');
                                    $('#dialog_warn_user').dialog('open');
                                }
                            }
            });
		}else{
                $('#dialog_warn_user').html('<b>Please check your input</b>');
                $('#dialog_warn_user').dialog('open');
            }
	}
    
    $(document).ready(function() {
        $( "#start_nwslttr" ).datepicker({
			dateFormat: "yy-mm-dd" ,
			firstDay: 1 ,
			minDate:0
		});
    });
</script>
