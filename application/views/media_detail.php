<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div style="max-height:500px;width:100%;float:left;overflow-x:scroll;">
	<div class="accordion" id="medset_table" style="width:99%;">
		<?php echo $mediane; ?>
	</div>
	<div align="right" style="margin-top:5px;">
		<input type="button" onclick="check_all(0)" value="Check ALL" />&nbsp;&nbsp;<input type="button" onclick="uncheck_all(0)" value="Clear ALL" />
	</div>
</div>

<script>	
	function showLoaderS()
	{
        jQuery('#blockeS').fadeIn(200);
    }
    function hideLoaderS()
	{
		jQuery('#blockeS').fadeOut(200);
    }
	function check_all(parame)
	{
		if(parame==0)
		{
			$('#medset_table .medsetdach').prop('checked', true);
		}
		else
		{
			$('#medset_table_'+parame+' .medsetdach').prop('checked', true);
		}
	}
	
	function uncheck_all(parame)
	{
		if(parame==0)
		{
			$('#medset_table .medsetdach').prop('checked', false);
		}
		else
		{
			$('#medset_table_'+parame+' .medsetdach').prop('checked', false	);
		}
	}
	
	function save_media()
	{
		var isi = [];
		$("input[brand='medset_detail[]']:checked").each(function (){
            isi.push((jQuery(this).val()));
        });
		
		showLoaderS();
		$.ajax({
				cache:false,
				type:'POST',
				url:'<?php echo site_url()?>/liste/save_media',
				data:{'isi':isi, 'comp_id':'<?php echo $compo;?>'},
				success:function(data) {
					hideLoaderS();
					if(data==1)
					{
						$('#dialog_warn').html('<b>Saved Successfully</b>')
						$('#dialog_warn').dialog('open');	
					}						
					else
					{
						$('#dialog_warn').html('<b>Saved Failed</b>')
// 						$('#dialog_warn').html(JSON.stringify(data))
						$('#dialog_warn').dialog('open');
					}
				}
		});
	}
</script>
