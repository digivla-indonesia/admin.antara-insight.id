<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

	<div class="CSSTableGenerator" style="width:100%">
        <table>
            <tr><td colspan=3>&nbsp;</td></tr>
            <tr>
                <td width="15%">Company</td>
                <td width="2%" align="center">:</td>
                <td width="83%"><?=$company_name;?></td>
            </tr>
            <tr>
                <td width="15%">Status</td>
                <td width="2%" align="center">:</td>
                <td width="83%"><?=$status;?></td>
            </tr>
            <tr>
                <td width="15%">Expired Date</td>
                <td width="2%" align="center">:</td>
                <td width="83%"><?=$expired;?></td>
            </tr>
            <tr>
                <td width="15%">Log Counter</td>
                <td width="2%" align="center">:</td>
                <td width="83%">
                    <table id="loggu">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>User ID</th>
                                <th>Counter</th>
                            </tr>
                        </thead>
                        <tbody>
            <?php if (count($list) > 0) {
                    foreach ($list as $row) { ?>
                            <tr>
                                <td><?=$row['username'];?></td>
                                <td><?=$row['userid'];?></td>
                                <td><?=$row['counter'];?></td>
                            </tr>
            <?php   }
                }else{ ?>
                            <tr><td colspan=3><i>no user log data found</i></td></tr>
            <?php   } ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan=3>Period : <?=$period;?></td>
                            </tr>
                        </tfoot>
                    </table>

                </td>
            </tr>
        </table>
	</div>
