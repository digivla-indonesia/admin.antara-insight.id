<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div align="center">
    <div id="tabs" style="width:96%;min-height:850px;">
        <ul>
            <li><a href="#tabs1">Register Company</a></li>
            <li><a href="<?=site_url();?>/register/get_register_user">Register User</a></li>
            <li><a href="<?=site_url();?>/register/get_register_newsletter">Register Newsletter</a></li>
        </ul>
        <div id="tabs1">
            <div class="CSSTableGenerator" style="width:80%">
                <form id="form_pass" enctype="multipart/form-data">
                    <table>
                        <tr><td colspan=3>Register Company</td></tr>
                        <tr>
                            <td width="20%">Status<font color="red">&nbsp;&nbsp;*</font></td>
                            <td width="2%" align="center">:</td>
                            <td width="78%">
                                <select id="compy_status" name="compy_status" onchange="get_status()" style="width:100px;height:22px;">
                                    <option value="ACTIVE">ACTIVE </option>
                                    <option value="TRIAL">TRIAL </option>
                                </select>
                                &nbsp;&nbsp;&nbsp;<i>"TRIAL" status will be expired in 2 weeks</i>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%">Company Name<font color="red">&nbsp;&nbsp;*</font></td>
                            <td width="2%" align="center">:</td>
                            <td width="78%"><input type="text" style="width:500px;height:25px;" name="compy_name" id="compy_name"/></td>
                        </tr>
                        <tr>
                            <td width="20%">Address<font color="red">&nbsp;&nbsp;*</font></td>
                            <td width="2%" align="center">:</td>
                            <td width="78%"><input type="text" style="width:500px;height:25px;" name="compy_address" id="compy_address"/></td>
                        </tr>
                        <tr>
                            <td width="20%">Phone<font color="red">&nbsp;&nbsp;*</font></td>
                            <td width="2%" align="center">:</td>
                            <td width="78%"><input type="text" style="width:500px;height:25px;" name="compy_phone" id="compy_phone"/></td>
                        </tr>
                        <tr>
                            <td width="20%">Contact Person<font color="red">&nbsp;&nbsp;*</font></td>
                            <td width="2%" align="center">:</td>
                            <td width="78%"><input type="text" style="width:500px;height:25px;" name="compy_contact" id="compy_contact"/></td>
                        </tr>
                        <tr>
                            <td width="20%">Email<font color="red">&nbsp;&nbsp;*</font></td>
                            <td width="2%" align="center">:</td>
                            <td width="78%"><input type="text" style="width:500px;height:25px;" name="compy_email" id="compy_email"/></td>
                        </tr>
                        <tr>
                            <td width="20%">Expired<font color="red">&nbsp;&nbsp;*</font></td>
                            <td width="2%" align="center">:</td>
                            <td width="78%">
                                <input type="text" style="width:100px;height:25px;" name="compy_expired" id="compy_expired"/>
                                <label id="explabel"></label>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%"><?php echo $this->lang->line('comp_logo')?><font color="red">&nbsp;&nbsp;*</font></td>
                            <td width="2%" align="center">:</td>
                            <td width="78%">
                                <input type="file" style="width:500px;height:25px;" name="compy_logo" id="compy_logo"/>
                                <br>(width:170px, height:70px)
                            </td>
                        </tr>
                    <!--
                        <tr>
                            <td width="20%"><?php //echo $this->lang->line('comp_color')?></td>
                            <td width="2%" align="center">:</td>
                            <td width="78%"><div id="colorSelector"><div style="background-color: rgb(201, 201, 209)"></div></div></td>
                        </tr>
                    -->
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right">
                                <input type="reset" class="ui-button" value="<?php echo $this->lang->line('reset')?>" />&nbsp;&nbsp;&nbsp;
                                <input type="button" class="ui-button" value="<?php echo $this->lang->line('save')?>" onclick="save_company()" />
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- dialog -->
<div id="dialog_warn" align="center"></div>
<div id="dialog_sucess" align="center"></div>
<div id="dialog_warn_user" align="center"></div>
<div id="dialog_sucess_user" align="center"></div>

<script>
	function get_status()
	{
		var state = $('#compy_status').val();
		if (state=='TRIAL')
			{
				$('#compy_expired').val('<?php echo date('Y-m-d', strtotime('+14 day', strtotime(date('Y-m-d'))));?>');
				$('#compy_expired').attr('disabled', true);
				$('#explabel').text('Status is "TRIAL" (valid for 2 weeks only)');
			}
		else
			{
				$('#compy_expired').attr('disabled', false);
				$('#compy_expired').val('');
				$('#explabel').text('');
			}
	}
    
    function save_company()
	{
		var compy_name		 = $("#compy_name").val();
		var compy_address 	 = $("#compy_address").val();
		var compy_phone		 = $("#compy_phone").val();
		var compy_contact	 = $("#compy_contact").val();
		var compy_email		 = $("#compy_email").val();
		var compy_expired	 = $("#compy_expired").val();
		var compy_logo		 = $("#compy_logo").val();

		/*
		var colore		 = $("#colorSelector").html();
		var	colore1		 = colore.split('<div style="background-color: ');
		var	colore2		 = colore1[1].split('"></div>');
		var compy_color  =	colore2[0];
		*/

		if (compy_name!='' && compy_logo!='' && compy_expired!='' && compy_email!='' && compy_address!='' && compy_phone!='' && compy_contact!='')
		{
			showLoader();
			$("#form_pass").ajaxSubmit({
                cache: false,
                type: 'POST',
                data: {'compy_color':0},
                url: '<?php echo site_url()?>/register/register_company',
                success: function(data) {
                    console.log(JSON.stringify(data));
					if (data==1){
						hideLoader();
                        $('#dialog_sucess').html('<b>Saved Successfully</b>');
                        $('#dialog_sucess').dialog('open');
                    } else if(data==4){
                        hideLoader();
                        $('#dialog_warn').html('<b>Company Name Already Used</b>');
                        $('#dialog_warn').dialog('open');
                    } else if (data==3){
						hideLoader();
                        $('#dialog_warn').html('<b>Logo Format is Invalid, Saved Failed</b>');
                        $('#dialog_warn').dialog('open');
                    } else if (data==0){
                        hideLoader();
                        $('#dialog_warn').html('<b>Email Not Valid</b>');
                        $('#dialog_warn').dialog('open');
                    }else{
                        hideLoader();
                        $('#dialog_warn').html('<b>Saved Failed</b>');
                        $('#dialog_warn').dialog('open');
                    }
                }
			});
		}
		else
		{
			$('#dialog_warn').html('<b>Check Your Input</b>');
			$('#dialog_warn').dialog('open');
		}
	}
    
	$('#tabs').tabs();
    
	$(document).ready(function(){
		$(document).tooltip();
        
        $( "#compy_expired" ).datepicker({
			dateFormat: "yy-mm-dd" ,
            firstDay: 1 ,
			minDate:0
		});
        
		$("#dialog_warn").dialog({
				height: 'auto',
				width: '400px',
				autoOpen: false,
				modal: true,
				position:'middle',
				title: 'Confirmation',
				buttons: {
				'Close': function() {
					$(this).dialog('close');
				}
				}
		});
        
		$("#dialog_sucess").dialog({
				height: 'auto',
				width: '400px',
				autoOpen: false,
				modal: true,
				position:'middle',
				title: 'Confirmation',
				buttons: {
				'Close': function() {
					$(this).dialog('close');
					document.getElementById('form_pass').reset();
				}
				}
		});
        
		$("#dialog_warn_user").dialog({
				height: 'auto',
				width: '400px',
				autoOpen: false,
				modal: true,
				position:'middle',
				title: 'Confirmation',
				buttons: {
				'Close': function() {
					$(this).dialog('close');
				}
				}
		});
        
		$("#dialog_sucess_user").dialog({
				height: 'auto',
				width: '400px',
				autoOpen: false,
				modal: true,
				position:'middle',
				title: 'Confirmation',
				buttons: {
				'Close': function() {
					$(this).dialog('close');
					document.getElementById('form_user_reg').reset();
				}
				}
		});

	});
</script>
