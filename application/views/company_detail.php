<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<?php foreach($det_comp->result() as $det) {?>
<style>
	.evo-pop {
    z-index:9999;
	}

	.ui-dialog {
		   overflow: visible;
	}
	.ui-dialog .ui-dialog-content {
		overflow: visible;
	}
</style>

<div align="center">
	<div>
			<pre></pre>
			<form id="form_company" name="form_company" enctype="multipart/form-data" >
				<table>
				<tr>
					<td width="30%">Company Name<font color="red">&nbsp;&nbsp;*</font></td><td width="10%" align="center">:</td>
					<td width="50%"><input type="text" value="<?php echo $det->comp_name;?>" style="width:400px;" name="compy_name" id="compy_name"/></td>
				</tr>
				<tr><td colspan=3>&nbsp;</td></tr>
				<tr>
					<td width="30%">Address<font color="red">&nbsp;&nbsp;*</font></td><td width="10%" align="center">:</td>
					<td width="50%"><input type="text" value="<?php echo $det->comp_address;?>" style="width:400px;" name="compy_address"  id="compy_address"/></td>
				</tr>
				<tr><td colspan=3>&nbsp;</td></tr>
				<tr>
					<td width="30%">Phone<font color="red">&nbsp;&nbsp;*</font></td><td width="10%" align="center">:</td>
					<td width="50%"><input type="text"value="<?php echo $det->comp_phone;?>" style="width:400px;" name="compy_phone" id="compy_phone"/></td>
				</tr>
				<tr><td colspan=3>&nbsp;</td></tr>
				<tr>
					<td width="30%">Contact Person<font color="red">&nbsp;&nbsp;*</font></td><td width="10%" align="center">:</td>
					<td width="50%"><input type="text" value="<?php echo $det->comp_contact;?>" style="width:400px;" name="compy_contact" id="compy_contact"/></td>
				</tr>
				<tr><td colspan=3>&nbsp;</td></tr>
				<tr>
					<td width="30%">Email<font color="red">&nbsp;&nbsp;*</font></td><td width="10%" align="center">:</td>
					<td width="50%"><input type="text" value="<?php echo $det->comp_email;?>" style="width:400px;" name="compy_email" id="compy_email"/></td>
				</tr>
				<tr><td colspan=3>&nbsp;</td></tr>
				<tr>
					<td width="30%">Registration Date</td><td width="10%" align="center">:</td>
					<td width="50%"><input disabled type="text" value="<?php echo $det->comp_join;?>" style="width:100px;" id="compy_join"/></td>
				</tr>
				<tr><td colspan=3>&nbsp;</td></tr>
				<tr>
					<td width="30%">Status<font color="red">&nbsp;&nbsp;*</font></td><td width="10%" align="center">:</td>
					<td width="50%">
						<select id="compy_status" name="compy_status" style="width:100px;" onchange="get_status();">
						<?php if($det->comp_status=='TRIAL') {?>
							<option <?php echo $det->comp_status=='ACTIVE'?'SELECTED':''; ?> value="ACTIVE">ACTIVE</option>
							<option <?php echo $det->comp_status=='INACTIVE'?'SELECTED':''; ?> value="INACTIVE">INACTIVE</option>
							<option <?php echo $det->comp_status=='TRIAL'?'SELECTED':''; ?> value="TRIAL">TRIAL</option>
						<?php } ?>
						<?php if($det->comp_status=='ACTIVE' or $det->comp_status=='INACTIVE') {?>
							<option <?php echo $det->comp_status=='ACTIVE'?'SELECTED':''; ?> value="ACTIVE">ACTIVE</option>
							<option <?php echo $det->comp_status=='INACTIVE'?'SELECTED':''; ?> value="INACTIVE">INACTIVE</option>
						<?php } ?>
						</select>
					</td>
				</tr>
				<tr><td colspan=3>&nbsp;</td></tr>
				<tr>
					<td width="30%">Expired<font color="red">&nbsp;&nbsp;*</font></td><td width="10%" align="center">:</td>
					<td width="50%"><input type="text" <?php echo $det->comp_status=='TRIAL'?'DISABLED':''; ?> value="<?php echo $det->comp_expired;?>" style="width:100px;" name="compy_expired" id="compy_expired"/>
						<label id="explabel"><?php echo $det->comp_status=='TRIAL'?'Expired Date is 2 weeks for Trial Status':''; ?></label>
					</td>
				</tr>
				<tr><td colspan=3>&nbsp;</td></tr>
				<tr>
					<td width="30%"><?php echo $this->lang->line('comp_logo')?></td><td width="10%" align="center">:</td>
					<td width="50%">
						<img src="<?php echo base_url()?>asset/images/<?php echo $det->comp_icon;?>" style="max-height:80px;">
						<input type="file" style="width:400px;" name="compy_logo" id="compye_logo"/>
						(width:170px , height:70px)
					</td>
				</tr>
				<tr><td colspan=3>&nbsp;</td></tr>
				<!--
				<tr>
					<td width="30%"><?php echo $this->lang->line('comp_color')?></td><td width="10%" align="center">:</td>
					<td width="50%"><div id="colorSelector" style="z-index:9999;"><div style="background-color: <?php echo $det->comp_coltheme;?>;"></div></div></td>
				</tr>
				<tr><td colspan=3>&nbsp;</td></tr>
				-->
				<tr>
					<td width="30%">Created By</td><td width="10%" align="center">:</td>
					<td width="50%"><input type="text" disabled value="<?php echo $det->create_by;?>" style="width:100px;" id="create_by"/></td>
				</tr>
				<input type="hidden" id="compy_id" name="compy_id" value="<?php echo $det->comp_id?>" />		
		</table>
		</form>	
	</div>
</div>

<script>
	$(document).ready(function(){	
		$( "#compy_expired" ).datepicker({
			dateFormat: "yy-mm-dd" ,
			minDate:0
		});		
	});
	
	$('#colorSelector').ColorPicker({
		color: '<?php echo $det->comp_coltheme;?>',
		showOn:'focus',
		onShow: function (colpkr) {
			$(colpkr).fadeIn(500);
			return false;
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$('#colorSelector div').css('backgroundColor', '#' + hex);
		}
	});
	
	function get_status()
	{
		var state = $('#compy_status').val();
		if(state=='TRIAL')
			{
				$('#compy_expired').val('<?php echo $det->comp_expired;?>');
				$('#compy_expired').attr('disabled', true);
				$('#explabel').text('Status Is TRIAL (2 weeks expired)');
			}
		else
			{
				$('#compy_expired').attr('disabled', false);
				$('#explabel').text('');
			}
	}
	
	function update_company()
	{
		var compy_name		 = $("#compy_name").val();
		var compy_address 	 = $("#compy_address").val();
		var compy_phone		 = $("#compy_phone").val();
		var compy_contact	 = $("#compy_contact").val();
		var compy_email		 = $("#compy_email").val();
		var compy_expired	 = $("#compy_expired").val();		
		var compye_logo		 = $("#compye_logo").val();
		var compy_id		 = $("#compy_id").val();
		//alert(compy_id);
		
		/*
		var colore		 = $("#colorSelector").html();
		var	colore1		 = colore.split('<div style="background-color: ');
		var	colore2		 = colore1[1].split('"></div>');
		var compy_color  =	colore2[0];
		*/
		
		if(compy_name!='' && compy_expired!='' && compy_email!='' && compy_address!='' && compy_phone!='' && compy_contact!='')
		{
			//document.getElementById("form_company").submit();
			$("#form_company").ajaxSubmit({
			cache:false,
			type:'POST',
			data:{'compye_logo':compye_logo,'compy_color':0},
			//data:{'compy_id':compy_id, 'compy_name':compy_name, 'compy_address':compy_address, 'compy_phone':compy_phone, 'compy_contact':compy_contact, 'compy_email':compy_email, 'compy_expired':compy_expired, 'compy_logo':compy_logo},
			url:'<?php echo site_url()?>/register/update_company',
			success:function(data) {
					if(data==1)
					{
						hideLoader();
						$('#dialog_warn').html('<b>Saved Successfully</b>');
						$('#dialog_warn').dialog('open');
					}					
					else if(data==4)
						{
							hideLoader();
							$('#dialog_warn').html('<b>Company Name Already Used</b>');
							$('#dialog_warn').dialog('open');
						}
										
					else if(data==3)
						{
							hideLoader();
							$('#dialog_warn').html('<b>Logo Format is Invalid, Saved Failed</b>');
							$('#dialog_warn').dialog('open');
						}
					else if(data==0)
						{
							hideLoader();
							$('#dialog_warn').html('<b>Email Not Valid</b>');
							$('#dialog_warn').dialog('open');
						}
					else
						{
							hideLoader();
							$('#dialog_warn').html('<b>Saved Failed</b>');
							$('#dialog_warn').dialog('open');
						}
				}
			});
		}
		else
		{
			$('#dialog_warn').html('<b>Check Your Input</b>');
			$('#dialog_warn').dialog('open');
		}

	}
	
</script>
<?php } ?>


