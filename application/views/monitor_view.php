<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div align="center">
    <div id="tabs" style="width:96%;min-height:650px;">
        <ul>
            <li><a href="#tabs1">Activity Log</a></li>
            <li><a href="<?php echo site_url()?>/monitor/notification">Notification</a></li>
            <li><a href="<?php echo site_url()?>/monitor/add_notification_form">Add Notification</a></li>
        </ul>
        <div id="tabs1">
            <div style="margin:5px 0 20px 10px;text-align:left;">
                Select period :
                <input type="text" style="width:100px;height:23px;text-align:center;" id="log_start" value="<?=date("Y-m-d", strtotime("-30 days"));?>"/>
                to
                <input type="text" style="width:100px;height:23px;text-align:center;" id="log_end" value="<?=date("Y-m-d");?>"/>
                &nbsp;&nbsp;
                <input type="button" value="Update" class="ui-button" style="padding:1px 10px;" id="logdate">
            </div>
            <div id="data_log">
                <table id="list_log"></table>
                <div id="log_pager"></div>
            </div>
        </div>
    </div>
</div>

<!-- dialog -->
<div id="log_view"></div>
<div id="log_notes"></div>

<script>
	$('#tabs').tabs();
	
	$(document).ready(function() {
		jQuery("#list_log").jqGrid({
            url: '<?php echo site_url()?>/monitor/get_list_log',
            mtype: "post",
            postData: {
                    sdate: function() { return $('#log_start').val(); },
                    edate: function() { return $('#log_end').val(); }
                },
            datatype: "json",
            colNames: ['No', 'Company Name', 'Status', 'Expired Date', 'Counter', 'View'],
            colModel: [
                        {name:'no', index:'no', width:24, align:"center", search:false, sortable:false, resizable:false},
                        {name:'comp_name', index:'comp_name', search:false},
                        {name:'status', index:'comp_status', width:32, search:false},
                        {name:'expired', index:'comp_expired', width:32, search:false},
                        {name:'counter', index:'counter', width:32, align:"right", search:false},
                        {name:'view', index:'view', width:24, align:"center", search:false, sortable:false, resizable:false}
                    ],
            rowNum: 25,
            width: 875,
            height: 462,
            rowList: [25,50,100],
            pager: '#log_pager',
            sortname: 'counter',
            viewrecords: true,
            //multiselect: true,
            caption:"Log Counter List"
		}).jqGrid('navGrid', '#log_pager',
                    {edit:false, add:false, del:false}, //options
                    {}, // add options
                    {}, // del options
                    {overlay:false}, // edit options
                    //	{sopt: ['bw','eq','ne','lt','le','gt','ge','ew','cn']} // search options
                    {sopt: ['cn']} // search options
                );
        // akhir setting jqgrid =====================================================	
		
        $(document).tooltip();
        
        $( "#log_start" ).datepicker({
            dateFormat: "yy-mm-dd",
            firstDay: 1,
            maxDate: 0,
            defaultDate: "-1m",
            changeMonth: true,
            numberOfMonths: 2,
            onClose: function( selectedDate ) {
                                $( "#log_end" ).datepicker( "option", "minDate", selectedDate );
                        }
        });
        
        $( "#log_end" ).datepicker({
            dateFormat: "yy-mm-dd",
            firstDay: 1 ,
            maxDate: 0,
            changeMonth: true,
            numberOfMonths: 2,
            onClose: function( selectedDate ) {
                                $( "#log_start" ).datepicker( "option", "maxDate", selectedDate );
                        }
        });
        
        $("#log_view").dialog({
				height: 'auto',
				width: '720px',
				autoOpen: false,
				modal: true,
				position:'top',
				title: 'Detail Information',
				buttons: {
                            'Close': function() {
                                            $(this).dialog('close');
                                            $('#list_log').trigger("reloadGrid");
                                        }
                        }
        });
        
        $("#log_notes").dialog({
				height: 'auto',
				width: '400px',
				autoOpen: false,
				modal: true,
				position: 'middle',
				title: 'Confirmation',
				buttons: {
                            'Close': function() {
                                            $(this).dialog('close');
                                        }
                    }
		});
        
        $('#logdate').click(function() {
            $("#list_log").trigger('reloadGrid');
        });
	});
	
	function detail_log(id) {
        var start_date = $('#log_start').val(),
            end_date = $('#log_end').val();
		$.ajax({
            cache: false,
            type: 'POST',
            url: '<?php echo site_url()?>/monitor/detail_log',
            data: {id:id, start:start_date, end:end_date},
            dataType: 'html',
            success: function(data) {
                            $('#log_view').html(data);
                            $('#log_view').dialog('open');
                        }
		});
	}
</script>
