<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div id="tabs2">
	<div id="data_nl">
        <table id="list_nl"></table>
		<div id="nl_pager"></div>
	</div>
</div>

<!-- dialog -->
<div id="dialog_nl_view"></div>
<div id="dialog_nl_delete"></div>

<script>
	$('#tabs').tabs();
	
	$(document).ready(function() {
		jQuery("#list_nl").jqGrid({
            url: '<?php echo site_url()?>/liste/get_list_newsletter',
            mtype: "post",
            datatype: "json",
            colNames: ['No', 'Company', 'Email', 'Start', 'Recurrence', 'Status', 'View', 'Delete'],
            colModel: [
                        {name:'no', index:'no', width:24, align:"center", search:false, sortable:false, resizable:false},
                        {name:'comp_name', index:'comp_name', search:false, width:160},
                        {name:'nl_email', index:'alert_email', search:false},
                        {name:'nl_start', index:'alert_start', align:"center", search:false, width:52},
                        {name:'nl_reps', index:'time_repeat', align:"center", search:false, width:52},
                        {name:'nl_status', index:'statuse', align:"center", search:false, width:48},
                        {name:'view', index:'view', width:32, align:"center", search:false, sortable:false, resizable:false},
                        {name:'delete', index:'delete', width:32, align:"center", search:false, sortable:false, resizable:false}
                    ],
            rowNum: 25,
            width: 875, 
            height: 462,
            rowList: [25,50,100],
            pager: '#nl_pager',
            sortname: 'alert_start',
            viewrecords: true,
            //multiselect: true,
            caption:"Newsletter List"
		}).jqGrid('navGrid', '#nl_pager',
                    {edit:false, add:false, del:false}, //options
                    {}, // add options
                    {}, // del options
                    {overlay:false}, // edit options
                    //{sopt: ['bw','eq','ne','lt','le','gt','ge','ew','cn']} // search options
                    {sopt: ['cn']} // search options
                );
        // akhir setting jqgrid =====================================================	
		
        $(document).tooltip();
		
        $("#dialog_nl_view").dialog({
				height: 'auto',
				width: '700px',
				autoOpen: false,
				modal: true,
				position:'top',
				title: 'Detail Newsletter',
				buttons: {		
                            'Change': function() {
                                            update_newsletter();
                                        },
                            'Close': function() {
                                            $(this).dialog('close');
                                            $('#list_nl').trigger("reloadGrid");
                                        }
                        }
        });
        
        $("#dialog_nl_delete").dialog({
				height: 'auto',
				width: '500px',
				autoOpen: false,
				modal: true,
				position:'middle',
				title: 'Delete Newsletter',
        });
	});
	
	function view_newsletter(id) {
		$.ajax({
            cache: false,
            type: 'POST',
            url: '<?php echo site_url()?>/liste/detail_newsletter',
            data: {id:id},
            success: function(data) {
                            $('#dialog_nl_view').html(data);
                            $('#dialog_nl_view').dialog('open');
                        }
		});
	}
	
	function delete_newsletter(id, detail) {
		$('#dialog_nl_delete').dialog({
			buttons: {
                        'Delete': function() {
                                        if (id!='')
                                        {
                                            $.ajax({
                                                cache: false,
                                                type: 'POST',
                                                url: '<?php echo site_url()?>/liste/delete_newsletter',
                                                data: {'id':id},
                                                success: function(data) {
                                                                if (data==1)
                                                                {
                                                                    $('#dialog_warn').html('<b>Delete Successfully</b>');
                                                                    $('#dialog_warn').dialog('open');
                                                                    $('#list_nl').trigger("reloadGrid");
                                                                }
                                                                else
                                                                {
                                                                    $('#dialog_warn').html('<b>Delete Failed</b>');
                                                                    $('#dialog_warn').dialog('open');
                                                                    $('#list_nl').trigger("reloadGrid");
                                                                }
                                                            }
                                            });
                                        }
                                        $(this).dialog('close');
                                    },
                        'Cancel': function() {
                                        $(this).dialog('close');
                                    }
                }
		});
		$('#dialog_nl_delete').html('<b>Are You Sure Delete This Newsletter? <br><font color=red>'+detail+'</font></b>');
		$('#dialog_nl_delete').dialog('open');
	}
</script>
