<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div align="center">
<div id="tabs" style="width:96%;min-height:650px;">
	<ul>
		<li><a href="#tabs1">List of Quote</a></li>
		<li><a href="<?php echo site_url()?>/spokeperson/add_quote">Add Quote</a></li>
	</ul>
	<div id="tabs1">
		<div id="data_comp">
		<table id="list_comp"></table>
				<div id="comp_pager"></div>
		</div>
	</div>
</div>
</div>	


<div id="dialog_list"></div>
<div id="dialog_warn" align="center"></div>
<div id="dialog_delete"></div>

<script>
	$('#tabs').tabs();
	
	$(document).ready(function(){
	
		jQuery("#list_comp").jqGrid({
		url:'<?php echo site_url()?>/spokeperson/get_list_quote',
		mtype : "post",
		datatype: "json",
		colNames:['NO','Full Name','Created Date', 'Edit', 'Delete'],
		colModel:[ 
			{name:'no',index:'no', width:7, align:"center",search:false}, 
			{name:'spoke_name',index:'spoke_name', width:60},
			{name:'spoke_date',index:'spoke_date', width:30, align:"center", search:false},
			{name:'edit',index:'edit', width:10, align:"center", search:false, sortable:false},
			{name:'delete',index:'delete', width:10, align:"center", search:false, sortable:false},
		], 
			rowNum:20,
			width: 830, 
			height: 400,
			rowList:[10,20,50,100],
			pager: '#comp_pager',
			sortname: 'comp_id',
			viewrecords: true,
			//multiselect: true,
			caption:"List"
		}).jqGrid('navGrid','#comp_pager',
		{edit:false,add:false,del:false}, //options
		{}, // add options
		{}, // add options
		{}, // del options
		{overlay:false}, // edit options
	//	{sopt: ['bw','eq','ne','lt','le','gt','ge','ew','cn']} // search options
		{sopt: ['cn']} // search options
		);
	// akhir setting jqgrid =====================================================	
		
	$(document).tooltip();
	
	$("#dialog_list").dialog({
			modal: true,
			height: 'auto',
			width: '700px',
			autoOpen: false,
			iframe: true,
			open: $(this).parent().appendTo($("form:first")),
			position:'top',
			title: 'Edit Quote',
			buttons: {		
			'Close': function() {
				$(this).dialog('close');
				$('#list_comp').trigger("reloadGrid");
			},
			'Change': function() 
				{	
					save_spoke2();
				}
			}
	});
	
	$("#dialog_warn").dialog({
				height: 'auto',
				width: '400px',
				autoOpen: false,
				modal: true,
				position:'middle',
				title: 'Confirmation',
				buttons: {		
				'Close': function() {
					$(this).dialog('close');
					$("#spokeperson_temp").html('');
				}
				}
		});
		
	});
	
	$("#dialog_delete").dialog({
				height: 'auto',
				width: '500px',
				autoOpen: false,
				modal: true,
				position:'middle',
				title: 'Delete Quote Name',
	});
	
	function edit_spoke(id) 
	{
		$.ajax({
		cache:false,
		type:'POST',
		url:'<?php echo site_url()?>/spokeperson/detail_spoke',
		data:{'label':id},
		success:function(data) {
				$('#dialog_list').html(data);
				$('#dialog_list').dialog('open');	
			}
		});		
	}
	
	function delete_spoke(label, id) 
	{
		$('#dialog_delete').dialog({
			buttons: {		
				'Cancel': function() {
					$(this).dialog('close');
				},
				'Delete': function() 
				{		
					if(id!='')
					{
						$.ajax({
						cache:false,
						type:'POST',
						url:'<?php echo site_url()?>/spokeperson/delete_spoke',
						data:{'id':id},
						success:function(data) {
								if(data==1)
								{
									$('#dialog_warn').html('<b>Saved Successfully</b>');
									$('#dialog_warn').dialog('open');	
									$('#list_comp').trigger("reloadGrid");
								}						
								else
								{
									$('#dialog_warn').html('<b>Saved Failed</b>');
									$('#dialog_warn').dialog('open');	
									$('#list_comp').trigger("reloadGrid");
								}
							}
						});			
					}
					$(this).dialog('close');
				} 
			}
		});
		$('#dialog_delete').html('<b>Are You Sure Delete This ? <br><font color=red>'+label+'</font></b>')
		$('#dialog_delete').dialog('open');		
	}
	
	function save_spoke2()
	{
		var isi = [];
		$("input[id='medset_detail2']:checked").each(function (){
            isi.push((jQuery(this).val()));
        });
		
		var fullname = $("#fullname2").val();
		//alert(isi);
		if( fullname!='' && isi!='')
		{			
			showLoader();
			$("#form_spoke2").ajaxSubmit({
			cache:false,
			type:'POST',
			url:'<?php echo site_url()?>/spokeperson/save_spokerperson',
			data:{'isi':isi, 'edit':'ok'},
			success:function(data) {
					if(data==1)
					{
						hideLoader();
						$('#dialog_warn').html('<b>Saved Successfully</b>');
						$('#dialog_warn').dialog('open');
					}
					
					else if(data==4)
					{
						hideLoader();
						$('#dialog_warn').html('<b>Username Already Used</b>');
						$('#dialog_warn').dialog('open');
					}
					else
					{
						hideLoader();
						$('#dialog_warn').html('<b>Saved Failed</b>');
						$('#dialog_warn').dialog('open');
					}
				}
			});
		}
		else
		{
			$('#dialog_warn').html('<b>Check Your Input</b>');
			$('#dialog_warn').dialog('open');
		}

	}
	
	
</script>
