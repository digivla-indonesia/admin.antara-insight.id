<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div id="tabs1">
	<div class="CSSTableGenerator" style="width:100%" >
		<?php foreach ($det_user->result() as $usr) {
				if($usr->usr_comp_level==4) 
					{
						echo "<script>$('#custom').show();</script>";
						$men_comp = $this->register_model->cek_menu_comp($usr->usr_comp_id);
					}
				else
					{
						echo "<script>$('#custom').hide();</script>";
					}
		?>
		<table>
			<form id="form_user_reg">
			<tr><td colspan=3>&nbsp;</td></tr>
			<tr>
				<td width="30%"><?php echo $this->lang->line('user_fullname')?><font color="red">&nbsp;&nbsp;*</font></td><td width="5%" align="center">:</td>
				<td width="50%"><input type="text" style="width:500px;height:25px;" value="<?php echo $usr->usr_comp_name;?>" id="fullname"/></td>
			</tr>
			<tr>
				<td width="30%"><?php echo $this->lang->line('email')?><font color="red">&nbsp;&nbsp;*</font></td><td width="5%" align="center">:</td>
				<td width="50%"><input type="text" style="width:500px;height:25px;" value="<?php echo $usr->usr_comp_email;?>" id="email"/></td>
			</tr>
			
			<tr>
				<td width="30%"><?php echo $this->lang->line('username')?><font color="red">&nbsp;&nbsp;*</font></td><td width="5%" align="center">:</td>
				<td width="50%"><input type="text" style="width:500px;height:25px;" value="<?php echo $usr->usr_comp_login ?>" id="username"/></td>
			</tr>
			
			<tr>
				<td width="30%"><?php echo $this->lang->line('password')?></td><td width="5%" align="center">:</td>
				<td width="50%"><input type="password" style="width:500px;height:25px;"  id="password1"/></td>
			</tr>
			
			<tr>
				<td width="30%"><?php echo $this->lang->line('confirm_pass')?></td><td width="5%" align="center">:</td>
				<td width="50%"><input type="password" style="width:500px;height:25px;" id="password2"/></td>
			</tr>
			
			<tr>
				<td width="30%"><?php echo $this->lang->line('company')?><font color="red">&nbsp;&nbsp;*</font></td><td width="5%" align="center">:</td>
				<td width="50%"><input type="text" disabled style="width:500px;height:25px;" value="<?php echo $usr->comp_name.'&nbsp;('.$usr->comp_status.')' ?>" id="username"/></td>
			</tr>
			
			<tr>
				<td width="30%"><?php echo $this->lang->line('acess_menu')?><font color="red">&nbsp;&nbsp;*</font></td><td width="5%" align="center">:</td>
				<td width="50%">
					<?php foreach($levele->result() as $lev){
					$checked = $usr->usr_comp_level==$lev->level_id?'checked':'';
					?>
					<input type="radio" <?php echo $checked?> name="acess" id="acess<?php echo $lev->level_id?>" value="<?php echo $lev->level_id?>"  onchange="get_acess()" />&nbsp;&nbsp;<?php echo $lev->level_name?>&nbsp;<?php echo $lev->level_desc?><br>
					<?php }?>
					<div id="custom" style="margin-left:20px;">
						Select Menu :<br>
							<ul id="custom_menu">
								<?php foreach($menune->result() as $me){
									$checked = '';
									 if($usr->usr_comp_level==4) 
									 {
										if($men_comp->num_rows() > 0)
										{
											$stace = false;
											foreach($men_comp->result() as $cuy)
											{
												if($cuy->menu_id==$me->menu_id)
													{$stace = true;}
											}
											$checked = $stace==true?'CHECKED':'';
										}
									 }
								?>
									<li><input class="menu" <?php echo $checked;?> type='checkbox' brand='menu_item[]' value='<?php echo $me->menu_id;?>'><?php echo $me->menu_name;?></li>
								<?php }?>
									<li><input type="button" onclick="all_custom()" value="ALL"></li>
							</ul>							
					<div>
				</td>
			</tr>
			
			<input type="hidden" id="usr_id" value="<?php echo $usr->usr_comp_id?>" />
			<input type="hidden" id="comp_id" value="<?php echo $usr->comp_id?>" />
			</form>
		</table>
		<?php } ?>
	</div>
</div>

<script>
	function get_acess()
	{
		if($('#acess4').is(':checked')==true)
		{
			$('#custom').show("slow");
		}
		else
		{
			$('#custom').hide("slow");
		}
	}
	
	function all_custom()
	{
		$('#custom_menu .menu').prop('checked', true);
	}
	
	function change_user()
	{
		var fullname		 = $("#fullname").val();
		var email 	 		 = $("#email").val();
		var username		 = $("#username").val();
		var password1	 	 = $("#password1").val();
		var password2	 	 = $("#password2").val();
		var usr_id		 	 = $("#usr_id").val();
		var comp_id		 	 = $("#comp_id").val();
		var cek_acess 		 = $("input:radio[name=acess]:checked").val();
		var isi = '';
		if(cek_acess==4)
			{
				isi = [];
				$("input[brand='menu_item[]']:checked").each(function (){
					isi.push((jQuery(this).val()));
				});
			}
		else
			{
				isi = cek_acess;
			}
		
		if(username!='' && $("input:radio[name=acess]").is(':checked') && isi!='' && fullname!='' && email!='' )
		{
			if(password1==password2)
			{				
				$.ajax({
				cache:false,
				type:'POST',
				url:'<?php echo site_url()?>/register/update_user',
				data:{'fullname':fullname, 'email':email, 'username':username, 'password':password1, 'usr_id':usr_id, 'comp_id':comp_id,'acess':isi},
				success:function(data) {
						if(data==1)
						{
							$('#dialog_warn').html('<b>Saved Successfully</b>');
							$('#dialog_warn').dialog('open');	
						}
						
						else
						{
							$('#dialog_warn').html('<b>Saved Failed</b>');
							$('#dialog_warn').dialog('open');	
						}
					}
				});
			}
			else
			{
				$('#dialog_warn').html('<b>Password Not Match</b>');
				$('#dialog_warn').dialog('open');
			}
		}
		else
		{
			$('#dialog_warn').html('<b>Check Your Input</b>');
			$('#dialog_warn').dialog('open');
		}

	}
	
</script>
