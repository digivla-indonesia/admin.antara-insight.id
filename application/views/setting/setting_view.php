<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<div align="center">
<div id="tabs" style="width:96%;min-height:850px;">
	<ul>
		<li><a href="#tabs1"><?php echo $this->lang->line('password')?></a></li>
	</ul>
	<div id="tabs1">
		<div class="setting_menu" style="width:50%">
			<table>
				<form id="form_pass">
				<tr>
					<td width="30%"><?php echo $this->lang->line('old_pass')?></td><td width="10%" align="center">:</td>
					<td width="50%"><input type="password" style="width:250px;" id="old_pass"/></td>
				</tr>
				<tr><td colspan=3>&nbsp;</td></tr>
				<tr>
					<td width="30%"><?php echo $this->lang->line('new_pass')?></td><td width="10%" align="center">:</td>
					<td width="50%"><input type="password" style="width:250px;" id="new_pass"/></td>
				</tr>
				<tr><td colspan=3>&nbsp;</td></tr>
				<tr>
					<td width="30%"><?php echo $this->lang->line('confirm_pass')?></td><td width="10%" align="center">:</td>
					<td width="50%"><input type="password" style="width:250px;" id="confirm_pass"/></td>
				</tr>
				<tr><td colspan=3>&nbsp;</td></tr>
				<tr>
					<td>&nbsp;</td><td>&nbsp;</td>
					<td align="right">
						<input type="reset" value="<?php echo $this->lang->line('reset')?>" />&nbsp;&nbsp;&nbsp;
						<input type="button" value="<?php echo $this->lang->line('save')?>" onclick="change_pass()" />
					</td>
				</tr>
				</form>
			</table>
		</div>
	</div>
</div>
</div>	


<div id="dialog_setting"></div>
<script>
	$('#tabs').tabs();
	
	$(function() {
        $( document ).tooltip();
    });
	
	function close_dialog()
	{
		$.fn.colorbox.close();
	}
	
	function change_pass()
	{
		var old_pass	 = $("#old_pass").val();
		var new_pass 	 = $("#new_pass").val();
		var confirm_pass = $("#confirm_pass").val();
		if(old_pass!='' && new_pass!='' && confirm_pass!='')
		{
			if(new_pass!=confirm_pass)
			{
				$("#dialog_setting").colorbox({html:'<font color=red><?php echo $this->lang->line('warn_pass2')?></font><br><br><input type="button" value="OK" onclick="close_dialog()" /><br>', open:true});
			}
			else
			{
				$.ajax({
				cache:false,
				type:'POST',
				url:'<?php echo site_url()?>/setting/change_pass',
				data:{'old_pass':old_pass, 'new_pass':new_pass},
				success:function(data) {
						if(data==1)
						{
							$("#dialog_setting").colorbox({html:'<font color=red><?php echo $this->lang->line('warn_pass4')?></font><br><br><input type="button" value="OK" onclick="close_dialog()" /><br>', open:true});
						}
						else if(data==0)
						{
							$("#dialog_setting").colorbox({html:'<font color=red><?php echo $this->lang->line('warn_pass3')?></font><br><br><input type="button" value="OK" onclick="close_dialog()" /><br>', open:true});
						}
						
						else
						{
							$("#dialog_setting").colorbox({html:'<font color=red><?php echo $this->lang->line('warn_pass5')?></font><br><br><input type="button" value="OK" onclick="close_dialog()" /><br>', open:true});
						}
					}
				});
			}			
		}
		else
		{
			$("#dialog_setting").colorbox({html:'<font color=red><?php echo $this->lang->line('warn_pass1')?></font><br><br><input type="button" value="OK" onclick="close_dialog()" /><br>', open:true});
		}

	}
	
</script>
