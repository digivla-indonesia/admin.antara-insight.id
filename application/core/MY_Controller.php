<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

/* The MX_Controller class is autoloaded as required */
class MY_Controller extends MX_Controller {
	function __construct() 
	{
		$unlocked = array('login');
		if ( !$this->site_sentry->is_logged_in() AND ! in_array(strtolower(get_class($this)), $unlocked))
		{
			redirect('login');
		}
	}
}