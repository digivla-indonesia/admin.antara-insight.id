<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

Class Setting extends MY_Controller 
{

	function __construct()
	{
		parent::__construct();
		$lange = $this->session->userdata('lang')=='eng'?'english':'indonesia';
		$this->lang->load('general',$lange);
		$this->load->model('setting_model');
		
		$arrayCSS = array (
		'asset/css/css.css',
		'asset/css/ui/jquery-ui-1.9.2.custom.min.css',
		'asset/css/colorbox/colorbox.css',
		);
		
		$arrayJS = array (
		'asset/javascript/core/jquery-1.8.3.js',
		'asset/javascript/core/jquery-ui-1.9.2.custom.min.js',	
		'asset/javascript/colorbox/jquery.colorbox-min.js',
		);
		
		$data['extraHeadContent'] = '';
		
		foreach ($arrayCSS as $css):
			$data['extraHeadContent'] .= '<link type="text/css" rel="stylesheet" href="'.base_url().$css.'"/>';
		endforeach;
		foreach ($arrayJS as $js):
			$data['extraHeadContent'] .= '<script type="text/javascript" src="'.base_url().$js.'"/></script>';
		endforeach;
		
		$this->load->vars($data);
	}

	function index()
	{
		$data['status'] 		  = 'setting';
		$data['content'] 		  = 'setting/setting_view';
		$this->load->view('main/index', $data);
	}

	
	//password
	function change_pass()
	{
		$old_pass 	= md5($this->input->post('old_pass'));
		$new_pass 	= md5($this->input->post('new_pass'));
		$usr_id		= $this->session->userdata('usr_agent_id'); 
		
		$cek_pass = $this->setting_model->cek_password($usr_id, $old_pass);
		$stat = 1;
		if($cek_pass->num_rows()>0)
		{
			$updt = $this->setting_model->update_password($usr_id, $new_pass);
			$stat = $updt==true?1:2; 
		}
		else
		{
			$stat =  0;
		}
		echo $stat;
	}
	

}
 
?>