<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

Class Monitor extends MY_Controller 
{

	function __construct() {
		parent::__construct();
		$lange = $this->session->userdata('lang')=='eng' ? 'english' : 'indonesia';
		$this->lang->load('general', $lange);
		$this->load->model('monitor_model');
		$this->load->model('register_model');
		
		$arrayCSS = array (
                            'asset/css/css.css',
                            'asset/css/ui/jquery-ui-1.9.2.custom.min.css',
                            'asset/css/ui/ui.jqgrid.css',
                            'asset/css/colorpicker/css/colorpicker.css',
                            'asset/css/colorpicker/css/layout.css',
                            'asset/css/table.css',
                        );
		
		$arrayJS = array (
                            'asset/javascript/core/jquery-1.8.3.js',
                            'asset/javascript/core/jquery.form.js',
                            'asset/javascript/core/jquery-ui-1.9.2.custom.min.js',
                            'asset/javascript/core/jquery.printElement.min.js',	
                            'asset/javascript/jqgrid/js/i18n/grid.locale-en.js',
                            'asset/javascript/jqgrid/jquery.jqGrid.min.js',
                            'asset/javascript/colorpicker/colorpicker.js',
                            'asset/javascript/colorpicker/eye.js',
                            'asset/javascript/colorpicker/utils.js',
                            'asset/javascript/colorpicker/layout.js',
                        );
		
		$data['extraHeadContent'] = '';
		
		foreach ($arrayCSS as $css):
			$data['extraHeadContent'] .= '<link type="text/css" rel="stylesheet" href="'.base_url().$css.'"/>';
		endforeach;
        
		foreach ($arrayJS as $js):
			$data['extraHeadContent'] .= '<script type="text/javascript" src="'.base_url().$js.'"/></script>';
		endforeach;
		
		$this->load->vars($data);
	}

	function index() {
		$data['status']  = 'monitor';
		$data['content'] = 'monitor_view';
        
		$this->load->view('main/index', $data);
	}
    
    function get_list_log() {
        $page   = isset($_POST['page']) ? $_POST['page'] : 1;   // get the requested page
		$limit  = isset($_POST['rows']) ? $_POST['rows'] : '';  // get how many rows we want to have into the grid
		$sidx   = isset($_POST['sidx']) ? $_POST['sidx'] : '';  // get index row - i.e. user click to sort
		$sord   = isset($_POST['sord']) ? $_POST['sord'] : '';  // get the direction
        
        $startd = isset($_POST['sdate']) ? $_POST['sdate'] : date("Y-m-d", strtotime("-30 days"));
        $endd   = isset($_POST['edate']) ? $_POST['edate'] : date('Y-m-d');
        
		$wh = '';
		$searchOn = $_POST['_search'];
        
		if ($searchOn == 'true') {
            $fld = $_POST['searchField'];
            
			if ($fld=='comp_name' || $fld=='comp_status' || $fld=='counter') {
				$fldata = $_POST['searchString'];
				$foper  = $_POST['searchOper'];
                
				switch ($fld) {
					case 'comp_name':
						$wh = 'b.comp_name';
                        break;
                    case 'comp_status':
						$wh = 'b.comp_status';
                        break;
					case 'counter':
						$wh = 'counter';
                        break;
				}
                
				switch ($foper) {
					case "cn":
						$like = " ".$wh." LIKE '%".$fldata."%' ";
						break;
					default :
						$like = " ".$wh." LIKE '%".$fldata."%' ";
				}
			}
		}else{
			$like = '';
		}
		
		if (!empty($sidx)) {
            if ($sidx == 'counter') {
                $sidx = 'ORDER BY '.$sidx.' DESC';
            }else{
                $sidx = 'ORDER BY '.$sidx.'';
            }
		}else{
			$sidx = '';
		}
		
		$limitNo = '';
		$qrychk  = $this->monitor_model->get_log_grid($like, $limitNo, $sidx, $startd, $endd);
		$chknum  = $qrychk->num_rows();
		
		if ( $chknum > 0 ) {
			$total_page = ceil($chknum/$limit);
		}else{
			$total_page = 0;
		}
		
		if ($page > $total_page) $page = $total_page;
		$start = $limit * $page - $limit;
        $start = ($start < 0) ? (0) : ($start);
        
		if ($searchOn == 'true') {
            $logres = $this->monitor_model->get_log_grid($like, $limitNo, $sidx, $startd, $endd);
        }else{
            $limit  = "LIMIT ".$start.",".$limit;
            $logres = $this->monitor_model->get_log_grid($like, $limit, $sidx, $startd, $endd);
        }
        
        $result = array();
        $result['page']     = $page;
        $result['total']    = $total_page;
        $result['records']  = $chknum;
        
        $i = 0;
        $idx = $start+1;
        foreach ($logres->result_array() as $row) {
            $result['rows'][$i]['id']   = $row['log_client'];
            $result['rows'][$i]['cell'] = array(
                                                $idx,
                                                $row['comp_name'],
                                                $row['comp_status'],
                                                $row['comp_expired'],
                                                $row['counter'],
                                                '<a href=\'javascript:void(0)\' onClick=\'detail_log("'.$row['log_client'].'")\'><img border=\'0\' src=\''.base_url().'asset/images/view.jpg\'></a>'
                                            );
            $i++;
            $idx++;
        }
        
        echo json_encode($result);
    }
    
    function detail_log() {
        $client_id  = $this->input->post('id');
        $dstart     = $this->input->post('start');
        $dend       = $this->input->post('end');
        
        $fetch  = $this->monitor_model->get_log_details($client_id, $dstart, $dend);
        if ($fetch->num_rows() > 0) {
            $detail = array();
            $i = 0;
            foreach ($fetch->result_array() as $row) {
                $detail['company_name'] = $row['comp_name'];
                $detail['status']       = $row['comp_status'];
                $detail['expired']      = $row['comp_expired'];
                $detail['period']       = $dstart.' - '.$dend;
                $detail['list'][$i]     = array(
                                                'userid'    => $row['userid'],
                                                'username'  => $row['username'],
                                                'counter'   => $row['counter']
                                            );
                $i++;
            }
            
            $this->load->view('detail_log_view', $detail);
        }
    }
    
    function notification() {
        $this->load->view('monitor_notification_view');
	}
	
	function get_list_notification() {
		$page   = isset($_POST['page'])?$_POST['page']:1;   // get the requested page
		$limit  = isset($_POST['rows'])?$_POST['rows']:'';  // get how many rows we want to have into the grid
		$sidx   = isset($_POST['sidx'])?$_POST['sidx']:'';  // get index row - i.e. user click to sort
		$sord   = isset($_POST['sord'])?$_POST['sord']:'';  // get the direction
        
		$wh = "";
		$searchOn = $_POST['_search'];
        
		if ($searchOn == 'true') {
            $fld = $_POST['searchField'];
            
			if ($fld=='comp_name' || $fld=='note_type' || $fld=='note_status') {
				$fldata = $_POST['searchString'];
				$foper  = $_POST['searchOper'];
                
				switch ($fld) {
					case 'comp_name':
						$wh = 'co.comp_name';
                        break;
					case 'note_type':
						$wh = 'nt.notify_type';
                        break;
                    case 'note_status':
						$wh = 'nt.notify_status';
                        break;
				}
                
				switch ($foper) {
					case "cn":
						$like = " ".$wh." LIKE '%".$fldata."%' ";
						break;
					default :
						$like = " ".$wh." LIKE '%".$fldata."%' ";
				}
			}
		}else{
			$like = '';
		}
		
		if (!empty($sidx)) {
			$sidx = 'ORDER BY '.$sidx.'';
		}else{
			$sidx = '';
		}
		
		$limitNo = '';
		$sqlc    = $this->monitor_model->get_notes_grid($like, $limitNo, $sidx);
		$count   = $sqlc->num_rows();
		
		if ( $count > 0 ) {
			$total_pages = ceil($count/$limit);
		}else{
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page = $total_pages;
		$start = $limit * $page - $limit;
        $start = ($start < 0) ? (0) : ($start);
        
		if ($searchOn == 'true') {
            $SQL = $this->monitor_model->get_notes_grid($like, $limitNo, $sidx);
        }else{
            $limit  = "LIMIT ".$start.",".$limit."";
            $SQL    = $this->monitor_model->get_notes_grid($like, $limit, $sidx);
        }
        
		$result = $SQL;
		$responce->page		= $page;
		$responce->total	= $total_pages;
		$responce->records	= $count;
        $i = 0;
        $idx = $start + 1;
        $ntype_arr = array(
                            'info' => 'Information',
                            'warn' => 'Warning',
                            'alrt' => 'Alert'
                        );
        $note_status = array('A' => 'ACTIVE', 'I' => 'INACTIVE');
        
		foreach ($result->result_array() as $row) {
            $note_period = $row['start_date'].' - '.$row['end_date'];
            $note_text   = (strlen($row['notify_text']) < 50) ? $row['notify_text'] : substr($row['notify_text'], 0, 47).'...';
            
			$responce->rows[$i]['note_id']  = $row['notify_id'];
            $responce->rows[$i]['cell']     = array(
                                                    $idx,
                                                    $row['comp_name'],
                                                    '['.$ntype_arr[$row['notify_type']].'] '.$note_text,
                                                    $note_period,
                                                    $note_status[$row['notify_status']],
                                                    '<a href=\'javascript:void(0)\' onClick=\'detail_note('.$row['notify_id'].')\'><img border=\'0\' src=\''.base_url().'asset/images/view.jpg\'></a>',
                                                    '<a href=\'javascript:void(0)\' onClick=\'delete_note('.$row['notify_id'].',"'.$ntype_arr[$row['notify_type']].' period '.$note_period.'")\'><img border=\'0\' src=\''.base_url().'asset/images/delete.jpg\'></a>',
                                                );
            $i++;
            $idx++;
		}
        
        echo json_encode($responce);
	}
    
    function add_notification_form() {
        $agent_id       = $this->session->userdata('agent_id'); 
		$data['comp']   = $this->register_model->get_company($agent_id);
        
		$this->load->view('monitor_add_notification', $data);
    }
    
    function add_notification() {
        $comp_tag   = $this->input->post('company');
        $nt_type    = $this->input->post('type');
        $nt_text    = $this->input->post('notification');
        $nt_start   = $this->input->post('start');
        $nt_end     = $this->input->post('end');
        $nt_status  = $this->input->post('status');
        
        $comboid    = explode('#@$%', $comp_tag);
		$comp_id    = $comboid[0];
		$client_id  = $comboid[1];
        $nt_text    = strip_tags($nt_text);
        $stats      = 0;
        
        $add_note = $this->monitor_model->insert_notification($comp_id, $nt_start, $nt_end, $nt_type, $nt_text, $nt_status);
        if ($add_note == true) {
            $stats = 1;
        }
        
        echo $stats;
    }
    
    function detail_notification() {
        $notify_id  = $this->input->post('id');
        $detqry     = $this->monitor_model->get_notification_detail($notify_id);
        
        if ($detqry->num_rows() > 0) {
            $detres = $detqry->row();
            
            $detail['note_id']      = $detres->notify_id;
            $detail['company_name'] = $detres->comp_name;
            $detail['note_type']    = $detres->notify_type;
            $detail['note_text']    = $detres->notify_text;
            $detail['note_start']   = $detres->start_date;
            $detail['note_end']     = $detres->end_date;
            $detail['note_status']  = $detres->notify_status;
            
            $this->load->view('detail_notification_view', $detail);
        }
    }
    
    function update_notification() {
        $nt_id      = $this->input->post('id');
        $nt_type    = $this->input->post('type');
        $nt_text    = $this->input->post('notification');
        $nt_start   = $this->input->post('start');
        $nt_end     = $this->input->post('end');
        $nt_status  = $this->input->post('status');
        $stats      = 0;
        
        $updqry = $this->monitor_model->update_notification($nt_id, $nt_type, $nt_text, $nt_start, $nt_end, $nt_status);
        if ($updqry == true) {
            $stats = 1;
        }
        
        echo $stats;
    }
    
    function delete_notification() {
        $note_id = $this->input->post('id');
		$stat = 0;
        
		if ($note_id != '') {
			$stat = ($this->monitor_model->delete_notification($note_id)) ? 1 : 0;
		}
        
		echo $stat;
    }
}
 
?>
