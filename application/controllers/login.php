<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

Class Login extends MY_Controller 
{

	function __construct()
	{
		parent::__construct();
		$this->lang->load('general','english');
		
		$arrayCSS = array (
		'asset/css/css.css',
		);
		
		$arrayJS = array (
		'asset/javascript/core/jquery-1.8.3.js',		
		);
		
		$data['extraHeadContent'] = '';
		
		foreach ($arrayCSS as $css):
			$data['extraHeadContent'] .= '<link type="text/css" rel="stylesheet" href="'.base_url().$css.'"/>';
		endforeach;
		foreach ($arrayJS as $js):
			$data['extraHeadContent'] .= '<script type="text/javascript" src="'.base_url().$js.'"/></script>';
		endforeach;
		
		$this->load->vars($data);
	}

	function index()
	{
		if($this->site_sentry->is_logged_in())
		{
			redirect('register');
		}
		else
		{
			$data['login_msg'] = '';
			$this->load->view('login/index', $data);
		}
	}
	
	function login_check()
	{
		$usr_login 		= $this->input->post('logine');
		$password		= $this->input->post('passwde');
		
		$usr_login 		= mysql_real_escape_string($usr_login);
		$password		= mysql_real_escape_string($password);
		$password   	= md5($password);
		if(!empty($usr_login) and !empty($password))
		{
			$this->site_sentry->login_routine($usr_login, $password);
		}
		else
		{
			redirect('login/login_fail/');
		}
	}
	
	function login_fail()
	{
		$data['login_msg'] = $this->lang->line('login_msg_fail');
		$this->load->view('login/index', $data);
	}
	
	function log_out()
	{
		$this->site_sentry->logout_routine();
	}

}
 
?>