<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

Class Liste extends MY_Controller 
{

	function __construct()
	{
        parent::__construct();
		$lange = $this->session->userdata('lang')=='eng'?'english':'indonesia';
		$this->lang->load('general',$lange);
		$this->load->model('liste_model');
		$this->load->model('register_model');
		
		$arrayCSS = array (
		'asset/css/css.css',
		'asset/css/ui/jquery-ui-1.9.2.custom.min.css',
		'asset/css/ui/ui.jqgrid.css',
		'asset/css/colorpicker/css/colorpicker.css',
		'asset/css/colorpicker/css/layout.css',
		'asset/css/table.css',
		);
		
		$arrayJS = array (
		'asset/javascript/core/jquery-1.8.3.js',
		'asset/javascript/core/jquery.form.js',
		'asset/javascript/core/jquery-ui-1.9.2.custom.min.js',
		'asset/javascript/core/jquery.printElement.min.js',	
		'asset/javascript/jqgrid/js/i18n/grid.locale-en.js',
		'asset/javascript/jqgrid/jquery.jqGrid.min.js',
		'asset/javascript/colorpicker/colorpicker.js',
		'asset/javascript/colorpicker/eye.js',
		'asset/javascript/colorpicker/utils.js',
		'asset/javascript/colorpicker/layout.js',
		);
		
		$data['extraHeadContent'] = '';
		
		foreach ($arrayCSS as $css):
			$data['extraHeadContent'] .= '<link type="text/css" rel="stylesheet" href="'.base_url().$css.'"/>';
		endforeach;
		foreach ($arrayJS as $js):
			$data['extraHeadContent'] .= '<script type="text/javascript" src="'.base_url().$js.'"/></script>';
		endforeach;
		
		$this->load->vars($data);
    }

	function index()
    { 
		$data['status'] 		  = 'list';
		$data['content'] 		  = 'list_view';
		$this->load->view('main/index', $data);
    }

	function liste_user()
	{
		$this->load->view('list_user_view');
	}

	function get_list_company()
    {

		$page   = isset($_POST['page'])?$_POST['page']:1;   // get the requested page
		$limit  = isset($_POST['rows'])?$_POST['rows']:'';  // get how many rows we want to have into the grid
		$sidx   = isset($_POST['sidx'])?$_POST['sidx']:'';  // get index row - i.e. user click to sort
        $sord   = isset($_POST['sord'])?$_POST['sord']:'';  // get the direction
        $wh = "";

		$searchOn = $_POST['_search'];
        
		if ($searchOn == 'true')
		{
            $fld = $_POST['searchField'];
			if ($fld=='comp_name' || $fld=='comp_status')
            {
				$fldata = $_POST['searchString'];
				$foper  = $_POST['searchOper'];
				switch ($fld) {
					case 'comp_name':
						$wh = 'comp_name';
                        break;
					case 'comp_status':
						$wh = 'comp_status';
                        break;
				}
				switch ($foper) {
					case "cn":
						$like = " ".$wh." LIKE '%".$fldata."%' ";
						break;
					default :
						$like = " ".$wh." LIKE '%".$fldata."%' ";
				}
			}
		}
		else
		{
			$like = '';
        }

		if (!empty($sidx))
		{
			$sidx = 'ORDER BY '.$sidx.'';
		}
		else
		{
			$sidx = '';
        }
        

		$limitNo = '';
		$sqlc    = $this->liste_model->get_company_grid($like, $limitNo, $sidx);
		$count   = $sqlc->num_rows();
		
		if ( $count > 0 )
		{
			$total_pages = ceil($count/$limit);
		}
		else 
		{
			$total_pages = 0;
        }

		
		if ($page > $total_pages) $page = $total_pages;
		$start = $limit * $page - $limit;
        $start = ($start < 0) ? (0) : ($start);

		if ($searchOn == 'true')
        {
            $SQL = $this->liste_model->get_company_grid($like, $limitNo, $sidx);
        }
		else
        {
            $limit  = "LIMIT ".$start.",".$limit."";
            $SQL    = $this->liste_model->get_company_grid($like, $limit, $sidx);
        }


		$result = $SQL;
		$responce->page		= $page;
		$responce->total	= $total_pages;
		$responce->records	= $count;
        $i = 0;


		foreach ($result->result_array() as $row) {
			$responce->rows[$i]['comp_id']  = $row['comp_id'];
            $responce->rows[$i]['cell']     = array(
                                                    $i+1,
                                                    $row['comp_name'],
                                                    $this->site_sentry->get_dpu_usage('017C', $row['client_id']),
                                                    $row['comp_expired'],
                                                    $row['comp_status'],
                                                    '<a href=\'javascript:void(0)\' onclick=\'detail_company('.$row['comp_id'].')\'><img border=\'0\' src=\''.base_url().'asset/images/view.jpg\'></a>',
                                                    '<a href=\'javascript:void(0)\' onclick=\'detail_media("'.$row['comp_id'].'_%&$#@'.$row['client_id'].'")\'><img border=\'0\' src=\''.base_url().'asset/images/view.jpg\'></a>',
                                                    '<a href=\'javascript:void(0)\' onclick=\'delete_user('.$row['comp_id'].',"'.$row['comp_name'].'", "'.$row['client_id'].'")\'><img border=\'0\' src=\''.base_url().'/asset/images/delete.jpg\'></a>'
                                                );
            $i++;
		}
        
        echo json_encode($responce);

    }
	
	function detail_company()
	{
		$comp_id 	= $this->input->post('comp_id');
		$det_comp 	= $this->liste_model->get_company_detail($comp_id);
		$data['det_comp'] 		  = $det_comp;
		$this->load->view('company_detail', $data);
	}
	
	function delete_company()
	{
		$comp_id 	= $this->input->post('comp_id');
		$client_id 	= $this->input->post('client_id');
		$stat = 0;
		if($comp_id!='')
		{
			$del = $this->liste_model->delete_company($comp_id);
			if($del){
				$stat = 1;
				
				$data_string_drop_table		= '{"token":"'.$token.'", ';
				$data_string_drop_table		.= '"media_id": '.$data['media_id'].'}';
				
				$data_api_token = $this->get_api_token();
				$token = $data_api_token->data;
				
				$drop_client_url			= 'http://10.10.10.4:8080/backup/drop_client/'.$token.'/'.$client_id;
				$drop_client_data_string	= '{}';
				$this->site_sentry->consume_api($drop_client_url, $drop_client_data_string, "DELETE");
				
				$drop_client_elastic_url			= 'http://10.10.10.4:8080/elastic/client/'.$token.'/'.$client_id;
				$drop_client_elastic_data_string	= '{}';
				$this->site_sentry->consume_api($drop_client_elastic_url, $drop_client_elastic_data_string, "DELETE");
				
				$delete_keyword_counter_url			= 'http://10.10.10.4:8080/primary/keyword_counter/'.$token.'/'.$client_id;
				$delete_keyword_counter_data_string	= '{}';
				$this->site_sentry->consume_api($delete_keyword_counter_url, $delete_keyword_counter_data_string, "DELETE");
				
				$delete_tbl_company_backup_counter_url			= 'http://10.10.10.4:8080/backup/tbl_company/'.$token.'/'.$comp_id;
				$delete_tbl_company_backup_data_string	= '{}';
				$this->site_sentry->consume_api($delete_tbl_company_backup_counter_url, $delete_tbl_company_backup_data_string, "DELETE");
				
				
			}
			// $del = $this->liste_model->delete_user($user_comp_id);
			// if($del)
			// {
			//	$stat = 1;
			//	$this->register_model->delete_menu_user($user_comp_id);
			// }
			
		}
		echo $stat;
	}
	
	function detail_media()
	{
		$compo			= explode('_%&$#@', $this->input->post('comp_id'));
		$comp_id		= $compo[0];
		$client_id		= $compo[1];
		//$code 		= '203';
		$code 			= '203B';
		$data 			= array("client_id"=>$client_id);
		$media_set 		= $this->site_sentry->get_data_api_homie($code, $data);
		
		// echo "<pre>";
		// print_r($media_set);
		// echo "</pre>";
		$result = '';
		// $result = var_dump($media_set);
		if($media_set['code']==00)
		{			
			$nume = 1;
			foreach($media_set['data'] as $key=>$meseto)
			{
				$res1 = '';
				$res2 = '';
				
				$resh = ' <h3><b>'.$key.'</b></h3><div id="medset_table_'.$nume.'"><table class="CSSTableGenerator">
					   <tr style="display:none;"><td style="width:70%"><b>Media Name</b></td><td align="center" style="width:30%"><b>Action</b></td></tr>
				';
				foreach($meseto as $meset)
				{
					$check = $meset['choosen']==1?'CHECKED':'';
					if($check=='CHECKED')
					{
					$res1 .= '<tr>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;'.$meset['media_name'].'</td>
								<td align="center"><input type="checkbox" '.$check.' brand="medset_detail[]" class="medsetdach" value="'.$meset['media_id'].'"></td>
							  </tr>';
					}
					else
					{
					$res2 .= '<tr>
								<td>&nbsp;&nbsp;&nbsp;&nbsp;'.$meset['media_name'].'</td>
								<td align="center"><input type="checkbox" '.$check.' brand="medset_detail[]" class="medsetdach" value="'.$meset['media_id'].'"></td>
								</tr>';
					}
				}
				$result .= $resh.''.$res1.''.$res2;
				$result .='<tr><td>&nbsp;</td><td style="text-align:center;"><a href="javascript:;" title="check" onclick="check_all('.$nume.')" ><img src="'.base_url().'/asset/images/check.jpg"/></a>&nbsp;&nbsp;<a href="javascript:;" title="uncheck" onclick="uncheck_all('.$nume.')" ><img src="'.base_url().'/asset/images/uncheck.jpg"/></a></td></tr></table></div>';
			}
		}
		$data['mediane'] = $result;
		$data['compo']   = $this->input->post('comp_id');
		$this->load->view('media_detail', $data);
	}
	
	function save_media()
	{
		$code = '203U';
		$compo			= explode('_%&$#@', $this->input->post('comp_id'));
		$comp_id		= $compo[0];
		$client_id		= $compo[1];
		$choosen  = $this->input->post('isi');
		$data = array("client_id"=>"".$client_id."", "media_id"=>$choosen);
		$media_set = $this->site_sentry->get_data_api_homie($code, $data);
		var_dump($media_set);
		$result = 0; 
		if($media_set['code']==00)
		{
			$result = 1;
		}
		echo $result;
	}
	
	function get_list_user()
	{
		$page   = isset($_POST['page'])?$_POST['page']:1;   // get the requested page
		$limit  = isset($_POST['rows'])?$_POST['rows']:'';  // get how many rows we want to have into the grid
		$sidx   = isset($_POST['sidx'])?$_POST['sidx']:'';  // get index row - i.e. user click to sort
		$sord   = isset($_POST['sord'])?$_POST['sord']:'';  // get the direction
        
		$wh = "";
		$searchOn = $_POST['_search'];
        
		if ($searchOn=='true')
		{
			$fld = $_POST['searchField'];
			if ($fld=='comp_name' || $fld=='usr_comp_name')
            {
				$fldata = $_POST['searchString'];
				$foper  = $_POST['searchOper'];
                
				switch ($fld) {
					case 'comp_name':
						$wh = 'comp_name';
                        break;
					case 'usr_comp_name':
						$wh = 'usr_comp_name';
                        break;
				}
                
				switch ($foper) {
					case "cn":
						$like = " ".$wh." LIKE '%".$fldata."%' ";
						break;
					default :
						$like = " ".$wh." LIKE '%".$fldata."%' ";
				}
			}
		}
		else
		{
			$like = '';
		}
		
		if (!empty($sidx))
		{
			$sidx = 'ORDER BY '.$sidx.'';
		}
		else
		{
			$sidx = '';
		}
		
		$limitNo = '';
		$sqlc    = $this->liste_model->get_user_grid($like, $limitNo, $sidx);
		$count   = $sqlc->num_rows();
		
		if ($count > 0)
		{
			$total_pages = ceil($count/$limit);
		}
		else
		{
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page = $total_pages;
		$start = $limit * $page - $limit;
        $start = ($start < 0)?(0):($start); 
        
		if ($searchOn=='true')
        {
            $SQL = $this->liste_model->get_user_grid($like, $limitNo, $sidx);
        }
		else
        {
            $limit  = "LIMIT ".$start.",".$limit."";
            $SQL    = $this->liste_model->get_user_grid($like, $limit, $sidx);
        }
        
		$result = $SQL ; 
		$responce->page		= $page;
		$responce->total	= $total_pages;
		$responce->records	= $count;
        $i=0;
        
		foreach ($result->result_array() as $row) {
			$responce->rows[$i]['usr_comp_id']  = $row['usr_comp_id'];
            $responce->rows[$i]['cell']         = array(
                                                        $i+1,
                                                        $row['usr_comp_name'],
                                                        $row['comp_name'].'&nbsp;('.$row['comp_status'].')',
                                                        $row['level_name'],
                                                        '<a href=\'javascript:void(0)\' onclick=\'detail_user('.$row['usr_comp_id'].')\'><img border=\'0\' src=\''.base_url().'/asset/images/view.jpg\'></a>',
                                                        '<a href=\'javascript:void(0)\' onclick=\'delete_user('.$row['usr_comp_id'].',"'.$row['usr_comp_name'].'")\'><img border=\'0\' src=\''.base_url().'/asset/images/delete.jpg\'></a>',
                                                    );
            $i++;
		}
        
        echo json_encode($responce);
	}
	
	function detail_user()
	{
		$user_comp_id       = $this->input->post('user_comp_id');
		$data['det_user']   = $this->liste_model->get_user_detail($user_comp_id);
		$data['menune']	 	= $this->register_model->get_menu_company();
		$data['levele']     = $this->register_model->get_level_company();
		
        $this->load->view('user_detail', $data);
	}
	
	function delete_user()
	{
		$user_comp_id 	= $this->input->post('id');
		$stat = 0;
		if($user_comp_id!='')
		{
			$del = $this->liste_model->delete_user($user_comp_id);
			if($del)
			{
				$stat = 1;
				$this->register_model->delete_menu_user($user_comp_id);
			}
		}
		echo $stat;
	}
	
///////////////////////////////////////////////////////////
    
    function liste_newsletter() {
		$this->load->view('list_newsletter_view');
	}
    
	function get_list_newsletter() {
        $page   = isset($_POST['page'])?$_POST['page']:1;   // get the requested page
		$limit  = isset($_POST['rows'])?$_POST['rows']:'';  // get how many rows we want to have into the grid
		$sidx   = isset($_POST['sidx'])?$_POST['sidx']:'';  // get index row - i.e. user click to sort
		$sord   = isset($_POST['sord'])?$_POST['sord']:'';  // get the direction
        
		$wh = "";
		$searchOn = $_POST['_search'];
        
		if ($searchOn=='true')
		{
			$fld = $_POST['searchField'];
			if ($fld=='comp_name' || $fld=='nl_email')
            {
				$fldata = $_POST['searchString'];
				$foper  = $_POST['searchOper'];
                
				switch ($fld) {
					case 'comp_name':
						$wh = 'co.comp_name';
                        break;
					case 'nl_email':
						$wh = 'al.alert_email';
                        break;
				}
                
				switch ($foper) {
					case "cn":
						$like = " ".$wh." LIKE '%".$fldata."%' ";
						break;
					default :
						$like = " ".$wh." LIKE '%".$fldata."%' ";
				}
			}
		}
		else
		{
			$like = '';
		}
		
		if (!empty($sidx))
		{
			$sidx = 'ORDER BY '.$sidx.'';
		}
		else
		{
			$sidx = '';
		}
		
		$limitNo = '';
		$sqlc    = $this->liste_model->get_newsletter_grid($like, $limitNo, $sidx);
		$count   = $sqlc->num_rows();
		
		if ($count > 0)
		{
			$total_pages = ceil($count/$limit);
		}
		else
		{
			$total_pages = 0;
		}
		
		if ($page > $total_pages) $page = $total_pages;
		$start = $limit * $page - $limit;
        $start = ($start < 0)?(0):($start); 
        
		if ($searchOn=='true')
        {
            $SQL = $this->liste_model->get_newsletter_grid($like, $limitNo, $sidx);
        }
		else
        {
            $limit  = "LIMIT ".$start.",".$limit."";
            $SQL    = $this->liste_model->get_newsletter_grid($like, $limit, $sidx);
        }
        
		$result = $SQL ; 
		$responce->page		= $page;
		$responce->total	= $total_pages;
		$responce->records	= $count;
        $i = 0;
        $nl_status = array('A' => 'ACTIVE', 'I' => 'INACTIVE');
        
		foreach ($result->result_array() as $row) {
            $email_list = str_replace(';', '<br>', $row['alert_email']);
			$responce->rows[$i]['nl_id']    = $row['alert_id'];
            $responce->rows[$i]['cell']     = array(
                                                    $i+1,
                                                    $row['comp_name'],
                                                    $email_list,
                                                    $row['alert_start'],
                                                    $row['time_repeat'],
                                                    $nl_status[$row['statuse']],
                                                    '<a href=\'javascript:void(0)\' onClick=\'view_newsletter('.$row['alert_id'].')\'><img border=\'0\' src=\''.base_url().'asset/images/view.jpg\'></a>',
                                                    '<a href=\'javascript:void(0)\' onClick=\'delete_newsletter('.$row['alert_id'].',"'.$row['comp_name'].' | '.$row['time_repeat'].' since '.$row['alert_start'].'")\'><img border=\'0\' src=\''.base_url().'asset/images/delete.jpg\'></a>',
                                                );
            $i++;
		}
        
        echo json_encode($responce);
    }
    
    function detail_newsletter() {
        $alert_id = $this->input->post('id');
        $detqry   = $this->liste_model->get_newsletter_detail($alert_id);
        
        if ($detqry->num_rows() > 0) {
            $detres = $detqry->row();
            
            $detail['alert_id']       = $detres->alert_id;
            $detail['company_name']   = $detres->comp_name;
            $detail['recipient_list'] = $detres->alert_email;
            $detail['start_date']     = $detres->alert_start;
            $detail['recc']           = $detres->timerange;
            $detail['status']         = $detres->statuse;
            
            $category_array = explode(';', $detres->category_set);
            
            $code       = '001';
            $comp_id    = $detres->comp_id;
            $client_id  = $detres->client_id;
            $data       = array();
            $ctgry_html = '';
            
            $client_set = $this->api_get_data_client($code, $comp_id, $client_id, $data);
            
            if ($client_set['code'] == 40) {
                $ctgry_html = '<li>*** <b style="color:red;">ERROR</b> : Cannot generate category. '.$client_set['message'].' ***</li>';
            }elseif ($client_set['code'] == 00) {
                foreach ($client_set['data'] as $ulist) {
                    $get_subcat = $this->get_sub_category($ulist['category_set'], $comp_id, $client_id);
                    $state      = ($get_subcat['cat_stat'] == 0) ? 'disabled="disabled"' : '';
                    $checked    = (in_array($ulist['category_set'], $category_array)) ? ' CHECKED' : '';
                    
                    $ctgry_html .= '<li><input type="checkbox" name="catlist[]" value="'.$ulist['category_set'].'" id="cat'.$ulist['category_set'].'" data-tag="'.$get_subcat['sub_tags'].'" '.$state.$checked.'>&nbsp;&nbsp;<label for="cat'.$ulist['category_set'].'">'.$ulist['descriptionz'].'</label>';
                    $ctgry_html .= $get_subcat['sub_list'];
                    $ctgry_html .= '</li>';
                }
            }else{
                    $ctgry_html = '<li>*** Empty list - category not found ***</li>';
                }
            
            $detail['category_list'] = $ctgry_html;
            $detail['reps']          = $this->register_model->get_time_repeat();
            
            $this->load->view('detail_newsletter_view', $detail);
        }
    }
    
    function update_newsletter() {
        $alert_id   = $this->input->post('id');
        $recipient  = $this->input->post('recipient');
        $start_date = $this->input->post('start');
        $time_freq  = $this->input->post('repeat');
        $status     = $this->input->post('status');
        $cat_set    = $this->input->post('catset');
        $cat_id     = $this->input->post('catid');
        
        $recipient  = strip_tags($recipient);
        $cat_set    = substr($cat_set, 0, -1);
        $cat_id     = substr($cat_id, 0, -1);
        $cat_id     = trim($cat_id);
        $stats      = 0;
        
        $register = $this->liste_model->update_newsletter($alert_id, $recipient, $cat_set, $cat_id, $time_freq, $start_date, $status);
        if ($register == true) {
            $stats = 1;
        }
        
        echo $stats;
    }
    
    function delete_newsletter() {
        $alert_id = $this->input->post('id');
		$stat = 0;
        
		if ($alert_id != '') {
			$stat = ($this->liste_model->delete_newsletter($alert_id)) ? 1 : 0;
		}
        
		echo $stat;
    }
    
    ////////////// API calls //////////////
    function api_get_data_client($code, $comp_id, $client_id, $data) {
		$result = false;
		$key    = $this->site_sentry->get_key();
		$uid    = $this->register_model->get_comp_uid($comp_id);
        $uidnum = $uid->num_rows();
        
        if ($uidnum == 0) {
            $result = array(
                            'code' => 40,
                            'message' => 'No user defined in the company.'
                        );
        }else{
                $uid = $uid->row()->usr_uid;
                
                if (!empty($code) and !empty($key) and !empty($uid) and is_array($data)) {
                    if ($code == '017C') {
                        $url = "http://192.168.10.119/mskapi/api-hommie.php?";
                    }else{
                            $url = "http://192.168.10.119/mskapi/api.php?";
                        }
                    
                    $serialize      = serialize($data);
                    $encode_data    = base64_encode($serialize);
                    $params         = array(
                                            'a'         => $code,
                                            'keys'      => $key,
                                            'uid'       => $uid,
                                            'client_id' => $client_id,
                                            'data'      => $encode_data
                                        );
                    $send_params    = http_build_query($params);
                    $result_raw     = $this->site_sentry->get_content_post($url, $params);
                    $result         = json_decode($result_raw, 1);
                }
            }
        
		return $result;	
	}
    
    function get_sub_category($category_set, $comp_id, $client_id) {
		$code = '002';
		$data = array("category_set" => $category_set);
		$sub_client = $this->api_get_data_client($code, $comp_id, $client_id, $data);
        //print_r($sub_client);
        $response = '';
        $response = array(
                        'sub_tags'  => '',
                        'cat_stat'  => 1,
                        'sub_list'  => ''
                    );
		
        if ($sub_client['code'] == 00 and $sub_client['data'] != NULL) {
            $response['sub_list'] = '<ul class="subcatlist">';
			foreach ($sub_client['data'] as $resulte) {
                $response['sub_tags'] .= $resulte['category_id'].'|';
                $response['sub_list'] .= '<li>~ '.$resulte['category_id'].'</li>';
            }
            $response['sub_list'] .= '</ul>';
		}else{
                $response['cat_stat'] = 0;
            }
        
        return $response;
	}
}
 
?>
