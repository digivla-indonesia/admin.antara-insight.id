<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

Class Spokeperson extends MY_Controller 
{

	function __construct()
	{
		parent::__construct();
		$lange = $this->session->userdata('lang')=='eng'?'english':'indonesia';
		$this->lang->load('general',$lange);
		$this->load->model('liste_model');
		$this->load->model('register_model');
		
		$arrayCSS = array (
		'asset/css/css.css',
		'asset/css/ui/jquery-ui-1.9.2.custom.min.css',
		'asset/css/ui/ui.jqgrid.css',
		'asset/css/colorpicker/css/colorpicker.css',
		'asset/css/colorpicker/css/layout.css',
		'asset/css/table.css',
		);
		
		$arrayJS = array (
		'asset/javascript/core/jquery-1.8.3.js',
		'asset/javascript/core/jquery.form.js',
		'asset/javascript/core/jquery-ui-1.9.2.custom.min.js',
		'asset/javascript/core/jquery.printElement.min.js',	
		'asset/javascript/jqgrid/js/i18n/grid.locale-en.js',
		'asset/javascript/jqgrid/jquery.jqGrid.min.js',
		'asset/javascript/colorpicker/colorpicker.js',
		'asset/javascript/colorpicker/eye.js',
		'asset/javascript/colorpicker/utils.js',
		'asset/javascript/colorpicker/layout.js',
		);
		
		$data['extraHeadContent'] = '';
		
		foreach ($arrayCSS as $css):
			$data['extraHeadContent'] .= '<link type="text/css" rel="stylesheet" href="'.base_url().$css.'"/>';
		endforeach;
		foreach ($arrayJS as $js):
			$data['extraHeadContent'] .= '<script type="text/javascript" src="'.base_url().$js.'"/></script>';
		endforeach;
		
		$this->load->vars($data);
	}

	function index()
	{
		$data['status'] 		  = 'spoke';
		$data['content'] 		  = 'spoke_view';
		$this->load->view('main/index', $data);
	}
	
	function add_quote()
	{
		$this->load->view('add_spoke_view');
	}
	
	function get_list_quote()
	{
		$page = isset($_POST['page'])?$_POST['page']:1; // get the requested page
		$limit = isset($_POST['rows'])?$_POST['rows']:''; // get how many rows we want to have into the grid
		$sidx = isset($_POST['sidx'])?$_POST['sidx']:''; // get index row - i.e. user click to sort
		$sord = isset($_POST['sord'])?$_POST['sord']:''; // get the direction
		if($page==1){$page=0;}
		
		$searchOn = $_POST['_search'];
		if($searchOn=='true')
		{
			$fld = $_POST['searchString'];
			$code = '205';		
			$data = array('cari'=>''.$fld.'', 'start'=>$page, 'limit'=>$limit);
			$datanee = $this->site_sentry->get_data_api_homie($code, $data);
  
		}
		else
		{
			$code = '205';		
			$data = array();
			$datanee = $this->site_sentry->get_data_api_homie($code, $data);
		}
   
     //var_dump($datanee);
		
		if($datanee['code']==00 and $datanee['data']!=NULL)
		{
			$total_pages = $datanee['total_row'];
			if($page > $total_pages) $page=$total_pages; 
			$i=0;
			foreach($datanee['data'] as $i => $row)
			{
				$responce->rows[$i]['label']=$row['label'];
				$responce->rows[$i]['cell']=array(
					$i+1,
					$row['label'],
					$datanee['timestamp'],
					'<a href=\'javascript:void(0)\' onclick=\'edit_spoke("'.$row['label'].'")\'><img border=\'0\' src=\''.base_url().'/asset/images/view.jpg\'></a>',
					'<a href=\'javascript:void(0)\' onclick=\'delete_spoke("'.$row['label'].'", '.$row['id'].')\'><img border=\'0\' src=\''.base_url().'/asset/images/delete.jpg\'></a>',
					);
				$i++;
			}
		}
   else
   {
       $responce = array('label'=>'','cell'=>array('','','','',''));
   }
    echo json_encode($responce);
	}
	
	function get_spokepersonByInitial()
	{
		$code = '205G';
		$spokeperson = $this->input->post('initial');
		
		$data = array("spokeperson"=>"".$spokeperson."");
		$datanee = $this->site_sentry->get_data_api_homie($code, $data);
		//var_dump($data);
		$temp = '<table id="medset_table">
			<tr bgcolor="#D6D6D6">
				<th style="text-align:center" width="90%"><b>Spoke Person</b></th>
				<th style="text-align:center" width="10%"><b>Action</b></th>
			</tr>
		';
		$count ="";
		if($datanee['code']==00 and $datanee['data']!=NULL)
		{
			$count = "About ".$datanee['total_row']." Spoke Person. (".round($datanee['exec_time'],4)." seconds)";
			foreach($datanee['data'] as $i => $row)
			{
				$check = $row['choosen']==1?'CHECKED':'';
				$temp .= '<tr>
							<td>'.$row['spokeperson'].'</td>
							<td align="center"><input type="checkbox" '.$check.'  id="medset_detail" class="medsetdach" value="'.$row['spokeperson'].'"></td>
						</tr>
					';
			}
			
		}
		else
		{
			$temp .= '<tr>
							<td colspan=2>'.$datanee['message'].'</td> 
						</tr>
					';
		}
		$temp .= '<tr>
							<td colspan=2 style="text-align:center"><input type="button" onclick="check_all(1)" value="Uncheck All" />&nbsp;
							<input type="button" onclick="check_all(0)" value="Check All" /></td> 
						</tr>
					';
		$temp .= '</table>';
		echo $temp;
	}
	
	function save_spokerperson()
	{		
		$fullname		= $this->input->post('fullname');
		$isi			= $this->input->post('isi');
		$id			= $this->input->post('id');
		$user_id		= $this->session->userdata('usr_agent_name');
		
		$stat = 0;
		if($fullname!='' and $isi!='' )
		{
			$namefile='';
			if(isset($_FILES['compy_logo']['name']))
			{
				$namefile = basename($_FILES['compy_logo']['name']);
				$type = basename($_FILES['compy_logo']['type']);
			}
			$stat_upl = false ;
			if($namefile!='')
			{
				$filename = ''.strtolower($fullname).".".$type;
				$folder = './asset/images/spokeperson/';
				$stat_upl = move_uploaded_file($_FILES['compy_logo']['tmp_name'], $folder.$filename);
			}
			else
			{
				$filename = '';
				$stat_upl = true ;
			}
			
			if($stat_upl)
			{
				$stat_ed = $this->input->post('edit');
				if($stat_ed=='edit' or $stat_ed=='ok')
				{
					$code = '205K';
					$data = array("id"=>"".$id."", "arr_spokeperson"=>$isi, "user"=>$user_id,);
					$comp_set = $this->site_sentry->get_data_api_homie($code, $data);
					$fullname_ori		= $this->input->post('fullname_ori');
					if($fullname!=$fullname_ori)
					{
						$data = array("id"=>"".$id."", "label"=>$fullname);
						$comp_set = $this->site_sentry->get_data_api_homie('205E', $data);
					}
					$stat = 1;
				}
				else
				{
					$code = '205C';
					$data = array("label"=>"".$fullname."", "arr_spokeperson"=>$isi, "user"=>$user_id, "jpg_file"=>$filename );
					$comp_set = $this->site_sentry->get_data_api_homie($code, $data);
					$stat = 1;
					//var_dump($data);
				}
			
			}
			else
			{
				$stat =  3;
			}
		}
		else
		{
			$stat =  4;
		}
		echo $stat;
	}
	
	function delete_spoke()
	{
		$label 	= $this->input->post('id');
		$stat = 0;
		if($label!='')
		{
			$code = '205J';
			$data = array("id"=>"".$label."");
			$media_set = $this->site_sentry->get_data_api_homie($code, $data);
			//var_dump($media_set);
			if($media_set['code']==00)
			{
				$stat = 1;
			}
		}
		echo $stat;
	}
	
	function detail_spoke()
	{
		$code = '205B';
		$spokeperson = $this->input->post('label');
		
		$data = array("label"=>"".$spokeperson."");
		$datanee = $this->site_sentry->get_data_api_homie($code, $data);
		//var_dump($datanee);
		$count ="";
		if($datanee['code']==00 and $datanee['data']!=NULL)
		{
		$temp = '<div class="CSSTableGenerator">
				<form id="form_spoke2">
				<table>
					<tr>
						<td width="40%">&nbsp;</td><td></td><td>
					</tr>
					<tr>
						<td >Full Name<font color="red">&nbsp;&nbsp;*</font></td><td width="5%" align="center">:</td>
						<td ><input type="text" value="'.$spokeperson.'" style="width:500px;height:25px;" name="fullname" id="fullname2" /></td>
					</tr>
					<tr>
						<td >Foto<font color="red">&nbsp;&nbsp;*</font></td><td width="5%" align="center">:</td>
						<td ><input  type="file" style="width:400px;" name="compy_logo" id="compye_logo"/></td>
					</tr>
					<tr>
						<td>&nbsp;</td><td>:</td><td><img src="'.base_url().'asset/images/spokeperson/'.$datanee['data'][0]['jpg_file'].'" style="width:150px;height:150px;"  ></td>
					</tr>	
					<tr>
						<td>&nbsp;</td><td></td><td>';
					
			$temp .= '<table id="medset_table2">
				<input type="hidden" name="fullname_ori" value="'.$spokeperson.'" />
				<input type="hidden" name="id" value="'.$datanee['data'][0]['id'].'" />
				<tr bgcolor="#D6D6D6">
					<th style="text-align:center" width="90%"><b>Spoke Person</b></th>
				</tr>
			';
			$count = "About ".$datanee['total_row']." Spoke Person. (".round($datanee['exec_time'],4)." seconds)";
			foreach($datanee['data'] as $i => $row)
			{
				$check = 'CHECKED';
				$temp .= '<tr>
							<td>'.$row['spokeperson'].'</td>
							<td align="center"><input type="checkbox" '.$check.'  id="medset_detail2" class="medsetdach2" value="'.$row['spokeperson'].'"></td>
						</tr>
					';
			}
			
		}
		else
		{
			$temp .= '<tr>
							<td>'.$datanee['message'].'</td> 
						</tr>
					';
		}
		$temp .= '</table>';
		$temp .= '</td></tr>
				 <!-- <tr>
						<td >Searching Name<font color="red">&nbsp;&nbsp;*</font><br>(Enter to show result)</td><td width="5%" align="center">:</td>
						<td ><input type="text" style="width:500px;height:25px;" id="cariSpoker"/></td>
				  </tr>
				  <tr>
					<td>&nbsp;</td><td></td><td><div id="spokeperson_temp2"></div></td>
				</tr>-->
				</table>
				</form>
			</div>';
		echo $temp;
	}
	


}
 
?>
