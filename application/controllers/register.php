<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

Class Register extends MY_Controller 
{

	function __construct()
	{
		parent::__construct();
		$lange = $this->session->userdata('lang')=='eng'?'english':'indonesia';
		$this->lang->load('general',$lange);
		$this->load->model('register_model');
		
		$arrayCSS = array (
		'asset/css/css.css',
		'asset/css/ui/jquery-ui-1.9.2.custom.min.css',
		'asset/css/colorpicker/css/colorpicker.css',
		'asset/css/colorpicker/css/layout.css',
		'asset/css/table.css',
		);
		
		$arrayJS = array (
		'asset/javascript/core/jquery-1.8.3.js',
		'asset/javascript/core/jquery-ui-1.9.2.custom.min.js',
		'asset/javascript/core/jquery.form.js',
		'asset/javascript/core/jquery.printElement.min.js',
		'asset/javascript/colorpicker/colorpicker.js',
		'asset/javascript/colorpicker/eye.js',
		'asset/javascript/colorpicker/utils.js',
		'asset/javascript/colorpicker/layout.js',
		);
		
		$data['extraHeadContent'] = '';
		
		foreach ($arrayCSS as $css):
			$data['extraHeadContent'] .= '<link type="text/css" rel="stylesheet" href="'.base_url().$css.'"/>';
		endforeach;
		foreach ($arrayJS as $js):
			$data['extraHeadContent'] .= '<script type="text/javascript" src="'.base_url().$js.'"/></script>';
		endforeach;
		
		$this->load->vars($data);
	}

	function index()
	{
		$data['status'] 		  = 'register';
		$data['content'] 		  = 'register_view';
		$this->load->view('main/index', $data);
	}
	
	function get_register_user()
	{
		$agent_id			  = $this->session->userdata('agent_id'); 
		$comp 				  = $this->register_model->get_company($agent_id);
		$menune 			  = $this->register_model->get_menu_company();
		$levele 			  = $this->register_model->get_level_company();
		$data['comp']	 	  = $comp;
		$data['menune']	 	  = $menune;
		$data['levele']	 	  = $levele;
		$this->load->view('register_user_view', $data);
	}
	
	function get_register_comp()
	{
		$data['comp_status']  = 'Active';		
		$this->load->view('register_company', $data);
	}
	
	function get_register_comptrial()
	{
		$data['comp_status']  = 'Trial';		
		$this->load->view('register_company', $data);
	}
	
	
	function register_company()
	{
		$this->load->helper('email');
		
		$comp_name 		= $this->input->post('compy_name');
		$comp_address 	= $this->input->post('compy_address');
		$comp_phone		= $this->input->post('compy_phone');
		$comp_contact	= $this->input->post('compy_contact');
		$comp_email		= $this->input->post('compy_email');
		$comp_expired	= $this->input->post('compy_expired');
		$compy_status	= $this->input->post('compy_status');
		$comp_color		= $this->input->post('compy_color');
		$agent_id		= $this->session->userdata('agent_id');
        
		$stat = 0;
		if(valid_email($comp_email))
		{
			$idne = '';
			$cek_comp = $this->register_model->cek_company($comp_name, $idne, $agent_id);
			if($cek_comp->num_rows()==0)
			{	
				if($compy_status=='TRIAL')
					{
						$comp_expired = date('Y-m-d', strtotime('+14 day', strtotime(date('Y-m-d'))));
					}
				
				$namefile = basename($_FILES['compy_logo']['name']);
				$stat_upl = false;
				if($namefile != '')
				{
					$rand = mktime();
					$filename = 'company/'.$rand.'_'.$namefile;
					$folder = './asset/images/';
					$stat_upl = move_uploaded_file($_FILES['compy_logo']['tmp_name'], $folder.$filename);
				}
				else
				{
					$filename = '';
				}
				
				if($stat_upl)
				{
					// $code = '201I';
					$data = array (
            			"agent_id"       => $agent_id, 
            			"company_name"   => "" . $comp_name . "", 
            			"address"        => "" . $comp_address ."",
            			"phone_number"   => "" . $comp_phone . "", 
            			"contact_person" => "" . $comp_contact . "",
            			"email"          => "" . $comp_email . "",
            			"expired_date"   => $comp_expired
          			);

					// $comp_set = $this->site_sentry->get_data_api_homie($code, $data);
					//$comp_set['code'] = 00;
					
					$comp_set = $this->site_sentry->get_data_api_homie('201IE', $data);
					// print_r($comp_set);
					
					$client_id  = $comp_set['data'];
					//$client_id  = 1;
					
          			if($comp_set['code']==00){
						$data = array(
							"client_id"	=> $comp_set['data']
						);
						$comp_set2 = $this->site_sentry->get_data_api_homie_new('201I', $data);
						// print_r($comp_set2);
						// remove comment dibawah ini kalau udah dibenerin rhesa masalah mysql date nya
						// if($comp_set2['code']==00){
							$ins = $this->register_model->insert_company($comp_name, $comp_address, $comp_phone, $comp_contact, $comp_email, $comp_color, $filename, $comp_expired, $compy_status, $client_id, $agent_id);
							$stat = $ins==true?1:2; 	
						// } else{
						//	$stat = 2;
						// }
						
					}
					else
					{
						$stat = 2 ;
					}
				}
				else
				{
					$stat =  3;
				}
			}
			else
			{
				$stat =  4;
			}
		}
		echo $stat;
	}
	
	function update_company()
	{
		$this->load->helper('email');
		
		$comp_name 		= $this->input->post('compy_name');
		$comp_address 	= $this->input->post('compy_address');
		$comp_phone		= $this->input->post('compy_phone');
		$comp_contact	= $this->input->post('compy_contact');
		$comp_email		= $this->input->post('compy_email');
		$comp_status	= $this->input->post('compy_status');
		$comp_expired	= $this->input->post('compy_expired');
		$comp_color		= $this->input->post('compy_color');
		$comp_id		= $this->input->post('compy_id'); 
		$comp_logo		= $this->input->post('compy_logo'); 
		$agent_id		= $this->session->userdata('agent_id');
	
		
		$stat = 0;
		if(valid_email($comp_email))
		{
			$cek_comp = $this->register_model->cek_company($comp_name, $comp_id, $agent_id);
			if($cek_comp->num_rows()==0)
			{
					$stat_upl = false;
					if($comp_logo!='')
					{
						$namefile = basename($_FILES['compy_logo']['name']);
						if($namefile!='')
						{
							$rand = mktime();
							$filename = 'company/'.$rand.'_'.$namefile;
							$folder = './asset/images/';
							$stat_upl = move_uploaded_file($_FILES['compy_logo']['tmp_name'], $folder.$filename);
					}
					}
					else
					{
						//print_r($filename);
						//$filename = '';
						$stat_upl = true;
						
					}
				/*
				$code = '201I';
				$data = array("agent_id"=>$agent_id, "company_name"=>"".$comp_name."", "address"=>"".$comp_address."", 
							"phone_number"=>"".$comp_phone."", "contact_person"=>"".$comp_contact."", "email"=>"".$comp_email."", 
							"expired_date"=>$comp_expired);
				$comp_set = $this->site_sentry->get_data_api_homie($code, $data);
				$client_id  = $comp_set['data'];*/
				if($stat_upl)
				{
					$upd = $this->register_model->update_company($comp_id, $comp_name, $comp_address, $comp_phone, $comp_contact, $comp_email, $comp_status,$comp_color, $comp_expired, $filename);
					$stat = $upd==true?1:2; 
				}
				else
				{
					$stat = 3;
				}
			}
			else
			{
				$stat =  4;
			}
		}
		echo $stat;
	}
	
	function register_user()
	{
		$this->load->helper('email');
		
		$fullname   = $this->input->post('fullname');
		$email      = $this->input->post('email');
		$username   = $this->input->post('username');
		$password   = md5($this->input->post('password'));
		$compo      = explode('#@$%', $this->input->post('company'));
		$comp_id    = $compo[0];
		$client_id  = $compo[1];
		$acess      = $this->input->post('acess'); 
		
		$stat = 0;
		if (valid_email($email))
		{
			$cek_user = $this->register_model->cek_user($username, $comp_id);
			if ($cek_user->num_rows()==0)
			{
				$code       = '202I';
				$data       = array("username"=>$username, "client_id"=>$client_id);
				$user_set   = $this->site_sentry->get_data_api_homie($code, $data);
				//var_dump($user_set);
				if ($user_set['code']==00)
				{
					$acessene = is_array($acess)?4:$acess;
					$usr_uid = $user_set['data'];
					$ins = $this->register_model->insert_user($fullname, $email, $username, $password, $comp_id, $acessene, $usr_uid);
					$stat = $ins==true?1:2;
					$usr_id = $this->db->insert_id();
					if ($acessene==4 and $ins==true)
					{
						foreach ($acess as $men)
						{
							$this->register_model->insert_menu_user($usr_id, $men);
						}
					}
				}
				else
				{
					//$stat =  3;
					var_dump($user_set);
					$stat = $user_set["data"];
				}
			}
			else
			{
				$stat =  4;
			}
		}
		echo $stat;
	}
	
	function update_user()
	{
		$this->load->helper('email');
		
		$fullname 		= $this->input->post('fullname');
		$email		 	= $this->input->post('email');
		$username		= $this->input->post('username');
		$usr_id			= $this->input->post('usr_id');
		$comp_id		= $this->input->post('comp_id');
		$password   	= $this->input->post('password')!=''?md5($this->input->post('password')):'';
		$acess		= $this->input->post('acess'); 
		
		$stat = 0;
		if(valid_email($email))
		{
			$cek_user = $this->register_model->cek_user($username, $comp_id, $usr_id);
			if($cek_user->num_rows()==0)
			{
				
				$acessene = is_array($acess)?4:$acess;
				$ins = $this->register_model->update_user($usr_id, $fullname, $email, $username, $acessene, $password);
				$stat = $ins==true?1:2; 
				//delete menu user dulu
				$del = $this->register_model->delete_menu_user($usr_id);
				if($acessene==4 and $ins==true)
				{
					foreach($acess as $men)
					{
						$this->register_model->insert_menu_user($usr_id, $men);
					}
				}
			}
			else
			{
				$stat =  4;
			}
		}
		echo $stat;
	}
    
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~//
    
    function get_register_newsletter() {
        $agent_id       = $this->session->userdata('agent_id'); 
		$data['comp']   = $this->register_model->get_company($agent_id);
        $data['reps']   = $this->register_model->get_time_repeat();
        
		$this->load->view('register_newsletter_view', $data);
    }
    
    function get_category_list() {
        $code       = '001';
        $comboid    = explode('#@$%', $this->input->post('compid'));
		$comp_id    = $comboid[0];
		$client_id  = $comboid[1];
        $data       = array();
        
        $html_result = '';
        $client_set = $this->api_get_data_client($code, $comp_id, $client_id, $data);
        //print_r($client_set);
        
        if ($client_set['code'] == 40) {
            $html_result = '<li>*** <b style="color:red;">ERROR</b> : Cannot generate category. '.$client_set['message'].' ***</li>';
        }elseif ($client_set['code'] == 00) {
            foreach ($client_set['data'] as $ulist) {
                $get_subcat = $this->get_sub_category($ulist['category_set'], $comp_id, $client_id);
                $state = ($get_subcat['cat_stat'] == 0) ? 'disabled="disabled"' : '';
                $html_result .= '<li><input type="checkbox" name="catlist[]" value="'.$ulist['category_set'].'" id="cat'.$ulist['category_set'].'" data-tag="'.$get_subcat['sub_tags'].'" '.$state.'>&nbsp;&nbsp;<label for="cat'.$ulist['category_set'].'">'.$ulist['descriptionz'].'</label>';
                $html_result .= $get_subcat['sub_list'];
                $html_result .= '</li>';
            }
        }else{
                $html_result = '<li>*** Empty list - category not found ***</li>';
            }
        
        echo $html_result;
    }
    
    function api_get_data_client($code, $comp_id, $client_id, $data) {
		$result = false;
		$key    = $this->site_sentry->get_key();
		$uid    = $this->register_model->get_comp_uid($comp_id);
        $uidnum = $uid->num_rows();
        
        if ($uidnum == 0) {
            $result = array(
                            'code' => 40,
                            'message' => 'No user defined in the company.'
                        );
        }else{
                $uid = $uid->row()->usr_uid;
                
                if (!empty($code) and !empty($key) and !empty($uid) and is_array($data)) {
                    if ($code == '017C') {
                        $url = "http://192.168.10.119/mskapi/api-hommie.php?";
                    }else{
                            $url = "http://192.168.10.119/mskapi/api.php?";
                        }
                    
                    $serialize      = serialize($data);
                    $encode_data    = base64_encode($serialize);
                    $params         = array(
                                            'a'         => $code,
                                            'keys'      => $key,
                                            'uid'       => $uid,
                                            'client_id' => $client_id,
                                            'data'      => $encode_data
                                        );
                    $send_params    = http_build_query($params);
                    $result_raw     = $this->site_sentry->get_content_post($url, $params);
                    $result         = json_decode($result_raw, 1);
                }
            }
        
		return $result;	
	}
    
    function get_sub_category($category_set, $comp_id, $client_id) {
		$code = '002';
		$data = array("category_set" => $category_set);
		$sub_client = $this->api_get_data_client($code, $comp_id, $client_id, $data);
        //print_r($sub_client);
        $response = '';
        $response = array(
                        'sub_tags'  => '',
                        'cat_stat'  => 1,
                        'sub_list'  => ''
                    );
		
        if ($sub_client['code'] == 00 and $sub_client['data'] != NULL) {
            $response['sub_list'] = '<ul class="subcatlist">';
			foreach ($sub_client['data'] as $resulte) {
                $response['sub_tags'] .= $resulte['category_id'].'|';
                $response['sub_list'] .= '<li>~ '.$resulte['category_id'].'</li>';
            }
            $response['sub_list'] .= '</ul>';
		}else{
                $response['cat_stat'] = 0;
            }
        
        return $response;
	}
    
    function register_newsletter() {
        $comp_tag   = $this->input->post('company');
        $recipient  = $this->input->post('recipient');
        $start_date = $this->input->post('start');
        $time_freq  = $this->input->post('repeat');
        $status     = $this->input->post('status');
        $cat_set    = $this->input->post('catset');
        $cat_id     = $this->input->post('catid');
        
        $comboid    = explode('#@$%', $comp_tag);
		$comp_id    = $comboid[0];
		$client_id  = $comboid[1];
        $recipient  = strip_tags($recipient);
        $cat_set    = substr($cat_set, 0, -1);
        $cat_id     = substr($cat_id, 0, -1);
        $cat_id     = trim($cat_id);
        $stats      = 0;
        
        $register = $this->register_model->add_newsletter($comp_id, $recipient, $cat_set, $cat_id, $time_freq, $start_date, $status);
        if ($register == true) {
            $stats = 1;
        }
        
        echo $stats;
    }
}
 
?>
