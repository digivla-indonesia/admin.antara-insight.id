<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

Class Site_sentry 
{
	
	private $username = 'digivla';
	private $password = 'nzVV2$/(zTH~>m3V';
	
	function Site_sentry()
	{
		$this->obj =& get_instance();
	}
	
	function get_api_token() {
    	$ch = curl_init('http://apps.antara-insight.id/token');
    	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    	curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    	    'Content-Type: application/json'
        )
	    );                                                         
    	$json = curl_exec($ch);
	    $result = json_decode($json);
    	return $result;
  	}
  	
  	function consume_api($data, $url, $method){
  		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_USERPWD, $this->username . ":" . $this->password);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',
			'Content-Length: ' . strlen($data))
		);
		$json = curl_exec($ch);
		$result = json_decode($json);

		return $result->data;
  	}

	function is_logged_in()
	{
		if ($this->obj->session) {
			if ($this->obj->session->userdata('logged_in'))
			{
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}		
	} 

	function login_routine($usr_login, $password)
	{
		$array = array('usr_agent_login' => $usr_login, 'usr_agent_passwd' => $password);
		$this->obj->db->where($array); 
		$query = $this->obj->db->get('tbl_user_agent');
		if($query->num_rows() == 1)
		{
			$user = $query->row()->usr_agent_name!=NULL?$query->row()->usr_agent_name:$query->row()->usr_agent_login;
			$user_id = $query->row()->usr_agent_id;
			$agent_id = $query->row()->agent_id;
			$usr_keys = $query->row()->usr_keys;
			$usr_uid = $query->row()->usr_uid;
			$usr_agent_level = $query->row()->usr_agent_level;
			$atribut	= $this->get_atribut_view($agent_id);
			$credentials = array('logged_in'=> '1', 'usr_agent_name'=> $user, 'agent_id'=> $agent_id, 'usr_agent_id'=>$user_id, 'usr_keys'=>$usr_keys,'usr_uid'=>$usr_uid, 'usr_agent_level'=>$usr_agent_level,'lang'=>'eng', 'logo_head'=>$atribut['logo_head'], 'col_theme'=>$atribut['col_theme']);
			$this->obj->session->set_userdata($credentials);
			redirect('register');
		}
		else
		{
			$credentials = array('usr_agent_name'=> '', 'logged_in'=> '');
			$this->obj->session->unset_userdata($credentials);
			redirect('login/login_fail');
		}
		
	}
	
	function get_dpu_usage($code, $client_id)
	{		
		$data = array();
		$datanee = $this->get_data_api_client($code, $client_id, $data);
		$response = '0';
		if($datanee['code']==00)
		{
			foreach($datanee['data'] as $has)
			{
				$response = $has['total_time'];
			}
		}
		return $response;
	}
	
	function get_atribut_view($id)
	{
		$this->obj->db->where('agent_id', $id); 
		$query = $this->obj->db->get('tbl_agent');
		$logo_head = $query->row()->agent_icon;
		$col_theme = $query->row()->agent_coltheme;
		
		$resulte = array('logo_head'=>$logo_head, 'col_theme'=>$col_theme);
		return $resulte;
	}
	
	function logout_routine()
	{
		$credentials = array('logged_in'=> '', 'usr_agent_name'=> '', 'usr_agent_id'=>'', 'usr_client_id'=>'','usr_keys'=>'','usr_uid'=>'', 'usr_agent_level'=>'','lang'=>'', 'logo_head'=>'', 'col_theme'=>'', 'agent_id'=>'');
		$this->obj->session->unset_userdata($credentials);		
		redirect('');
	}
	
	function get_menu()
	{
		$result = false;
		if($this->is_logged_in())
		{
			$this->obj->db->where('menu_stat', 'ACTIVE');
			$this->obj->db->order_by('menu_sorter');
			$result = $this->obj->db->get('tbl_menu_agent');
		}
		return $result;
	}
	
	function get_api_adrress()
	{
		
	}
	
	function get_key()
	{		
		$key  = $this->obj->session->userdata('usr_keys');
		return $key; 
	}	
	
	function get_uid()
	{		
		$uid  = $this->obj->session->userdata('usr_uid');
		return $uid; 
	}
	
	function get_client_id()
	{		
		$uid  = $this->obj->session->userdata('usr_client_id');
		return $uid; 
	}
	
	function get_data_api_client($code, $client_id, $data)
	{
		$result 	= false;
		$key    	= $this->get_key();
		$uid    	= $this->get_uid();
		
		if(!empty($code) and !empty($key) and !empty($uid) and is_array($data))
		{
			if($code=='017C')
			{
				// $url = "http://192.168.10.119/mskapi/api-hommie.php?";
				$url = "http://127.0.0.1/api-hommie.php?";
			}
			else
			{
				$url = "http://127.0.0.1/api.php?";
			}
			$serialize      = serialize($data); 
			$encode_data     = base64_encode($serialize);
			$params = array(
				"a" => ''.$code.'',
				"keys" => ''.$key.'',
				"uid" => ''.$uid.'',
				"client_id"=>''.$client_id.'',
				"data" => $encode_data,
			);
			$send_params = http_build_query($params);
			$result_raw = $this->get_content_post($url,$params );
			$result		 = json_decode($result_raw,1);
		}
		return $result;	
	}
	
	function get_data_api_homie($code, $data)
	{
		$result 	= false;

		$key    	= $this->get_key();
		$uid    	= $this->get_uid();
		
		if(!empty($code) and !empty($key) and !empty($uid) and is_array($data))
		{
			// $url = "http://192.168.10.119/mskapi/api-hommie.php?";
			$url = "http://mskapi.antara-insight.id/api-hommie.php?";
			$serialize      = serialize($data); 
			$encode_data     = base64_encode($serialize);
			$params = array(
				"a" => ''.$code.'',
				"keys" => ''.$key.'',
				"uid" => ''.$uid.'',
				"data" => $encode_data,
			);
			$send_params = http_build_query($params);
			$result_raw = $this->get_content_post($url,$params );
			$result		 = json_decode($result_raw,1);
		}
		return $result;	
	}
	
	function get_data_api_homie_new($code, $data)
	{
		$result 	= false;

		$key    	= $this->get_key();
		$uid    	= $this->get_uid();
		
		if(!empty($code) and !empty($key) and !empty($uid) and is_array($data))
		{
			// $url = "http://192.168.10.119/mskapi/api-hommie.php?";
			// $url = "http://127.0.0.1/api-hommie.php?";
			$url = "http://mskapi.antara-insight.id/api-hommie-new.php?";
			$serialize      = serialize($data); 
			$encode_data     = base64_encode($serialize);
			$params = array(
				"a" => ''.$code.'',
				"keys" => ''.$key.'',
				"uid" => ''.$uid.'',
				"data" => $encode_data,
			);
			$send_params = http_build_query($params);
			$result_raw = $this->get_content_post($url,$params );
			$result		 = json_decode($result_raw,1);
		}
		return $result;	
	}
	
	function get_content_post($url,$data ) 
	{ 
		$ch = curl_init();
		curl_setopt ($ch, CURLOPT_URL, $url); 
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt ($ch, CURLOPT_TIMEOUT, 49);
		ob_start();
		curl_exec ($ch);
		curl_close ($ch);
		$string = ob_get_contents();
		ob_end_clean();
		return $string;
	}
	
	 //preg_replace("/\w*?$keyword\w*/i", "<b>$0</b>", $str)
	function get_highlight($text, $keywords) 
	{ 
		$result = $text;
		$wordsplit_pattern = '/([\s\-_,:;?!\/\(\)\[\]{}<>\r\n"]|(?<!\d)\.(?!\d))/';
		$wordsplitkeys = preg_split($wordsplit_pattern,$keywords, null, PREG_SPLIT_NO_EMPTY);
		if($wordsplitkeys !== null)
		{
			if(count($wordsplitkeys) > 0)
			{  
				foreach($wordsplitkeys as $ii => $mystr)
				{
					//$result = str_ireplace($mystr,"<font style='background-color:#E4FF4A; font-weight:600;'>".$mystr."</font>",$result );
					$result = preg_replace("/\w*?".$mystr."\w*/i","<font style='background-color:#E4FF4A; font-weight:550;'>$0</font>",$result );
				}
			}
		}
		else
		{
			//$result = str_ireplace($keywords,"<font style='background-color:#E4FF4A; font-weight:600;'>".$keywords."</font>",$result);
			$result = preg_replace("/\w*?".$keywords."\w*/i","<font style='background-color:#E4FF4A; font-weight:550;'>$0</font>",$result );
		}
		return $result;
	}
	
    function get_highlight_array($text, $keywordse) 
    {
			
		$result = $text;
		$wordsplit_pattern = '/([\s\-_,:;?!\/\(\)\[\]{}<>\r\n"]|(?<!\d)\.(?!\d))/'; 
		foreach($keywordse as $keywords)
		{
			$wordsplitkeys = preg_split($wordsplit_pattern,$keywords, null, PREG_SPLIT_NO_EMPTY);
			if($wordsplitkeys !== null)
			{
				if(count($wordsplitkeys) > 0)
				{  
					foreach($wordsplitkeys as $ii => $mystr)
					{
						$result = preg_replace("/\w*?".$mystr."\w*/i","<font style='background-color:#E4FF4A; font-weight:550;'>$0</font>",$result );
					}
				}
			}
			else
			{
				$result = preg_replace("/\w*?".$keywords."\w*/i","<font style='background-color:#E4FF4A; font-weight:550;'>$0</font>",$result );
			}
		}
	   return $result;
	}

}
?>
